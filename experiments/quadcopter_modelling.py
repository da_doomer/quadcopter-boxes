"""Modelling the quadcopter dynamics with a pretrained controller."""
import argparse
from pathlib import Path
from control.neuroevolution import get_untrained_policy_network
from control.neuroevolution import get_random_initial_state
from control.neuroevolution import get_controller
from control.neuroevolution import get_controller_states
from control.neuroevolution import STEP_N
from control.clustering import get_state_vector
from world_modelling.mlp import get_optimized_world_model
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import torch
import random


def main(
        controller_path: Path,
        simulation_n: int,
        output_path: Path,
        iter_n: int,
        seed: str,
        device: str,
        batch_size: int,
        ):
    """Train a model of the quadcopter dynamics given a controller."""
    # Open controller
    network = get_untrained_policy_network()
    network.load_state_dict(torch.load(controller_path))

    # Gather trajectories of maximum length with the controller
    trajectories = list()
    while len(trajectories) < simulation_n:
        state = get_random_initial_state()
        controller = get_controller(network)
        states = get_controller_states(controller, state)
        # Ignore garbage trajectories
        if len(states) != STEP_N:
            print('skipping trajectory of incorrect length')
            continue
        trajectory = [get_state_vector(x) for x in states]
        trajectory = trajectory[:60]
        trajectories.append(trajectory)

    # Optimize world model
    result = get_optimized_world_model(
        trajectories=trajectories,
        encoded_size=128,
        mlp_hidden_sizes=[128, 128],
        bias=True,
        min_batch_size=batch_size,
        learning_rate=0.001,
        iter_n=iter_n,
        seed=seed,
        verbose=True,
        device=device,
        window_size=5,
    )

    # Plot losses
    plotting_data = [
        ("Forecasting loss (train)", result.losses.forecasting_losses),
    ]
    for name, losses in plotting_data:
        losses_path = output_path/f"{name}.svg"
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.plot(losses)
        fig.savefig(losses_path)
        print(f"Wrote {losses_path}")

    # Plot model predictions in training data
    _random = random.Random(seed)
    plot_prediction_n = min(10, len(trajectories))
    test_trajectories = _random.sample(trajectories, k=plot_prediction_n)
    n_given_step = 10
    for i, trajectory in enumerate(test_trajectories):
        state_size = len(trajectory[0])
        y_given = trajectory[:n_given_step]
        y_pred = y_given + result.model.predict(
            y_given,
            len(trajectory)-len(y_given),
            device=device,
        )
        y_target = trajectory
        y_pred = torch.tensor(y_pred)
        y_target = torch.tensor(y_target)
        y_given = torch.tensor(y_given)
        plot_path = output_path/f"prediction_example_{i}.svg"
        my_dpi = 256  # Matplotlib's default DPI (100) is too low
        fig = Figure(figsize=(8, 6*state_size), dpi=my_dpi)
        FigureCanvas(fig)
        axs = fig.subplots(nrows=state_size)
        for i in range(state_size):
            axs[i].plot(y_pred[:, i].tolist(), label="prediction")
            axs[i].plot(y_target[:, i].tolist(), label="target")
            axs[i].plot(y_given[:, i].tolist(), label="given")
            axs[i].legend()
        fig.savefig(plot_path)
        print(f"Wrote {plot_path}")

    # Serialize model
    model_dir = output_path/"model"
    model_dir.mkdir()
    result.model.save(model_dir)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Train a model of the quadcopter dynamics given a controller.',
    )
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--simulation-n',
        type=int,
        help='Number of simulations per test.',
        required=True,
    )
    parser.add_argument(
        '--optimization-iter-n',
        type=int,
        help='Number of gradient descent steps',
        required=True,
    )
    parser.add_argument(
        '--output-path',
        type=Path,
        help='Number of simulations per test.',
        required=True,
    )
    parser.add_argument(
        '--batch-size',
        type=int,
        help='Number of trajectories over which loss is computed on each optimization step.',
        required=True,
    )
    parser.add_argument(
        '--device',
        type=str,
        help='Training device (e.g., "cpu" or "cuda").',
        required=True,
    )
    args = parser.parse_args()
    args.output_path.mkdir()
    main(
        controller_path=args.controller_path,
        simulation_n=args.simulation_n,
        output_path=args.output_path,
        iter_n=args.optimization_iter_n,
        seed="asd",
        device=args.device,
        batch_size=args.batch_size,
    )
