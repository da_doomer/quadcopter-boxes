"""Modelling the quadcopter dynamics with a pretrained controller.

The dynamics model is a Hidden Markov Model. The hypothesis is that
the hidden states correspond to the modes of operation of the system.
"""
import argparse
from pathlib import Path
from control.neuroevolution import get_untrained_policy_network
from control.neuroevolution import get_random_initial_state
from control.neuroevolution import get_controller
from control.neuroevolution import get_controller_states
from control.neuroevolution import STEP_N
from control.clustering import get_state_vector
from world_modelling.normalization import get_normalizer
from control.clustering import plot_cluster_visualization
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from hmmlearn import hmm
import numpy as np
import torch
import random


def main(
        controller_path: Path,
        simulation_n: int,
        output_path: Path,
        seed: str,
        ):
    """Train a model of the quadcopter dynamics given a controller."""
    # Open controller
    network = get_untrained_policy_network()
    network.load_state_dict(torch.load(controller_path))

    # Gather trajectories of maximum length with the controller
    trajectories = list()
    trajectory_states = list()
    while len(trajectories) < simulation_n:
        state = get_random_initial_state()
        controller = get_controller(network)
        states = get_controller_states(controller, state)
        # Ignore garbage trajectories
        if len(states) != STEP_N:
            print('skipping trajectory of incorrect length')
            continue
        # DEBUGGING: only consider the initial part of the trajectory
        #states = states[:60]
        # /DEBUGGING
        trajectory = [get_state_vector(x) for x in states]
        trajectory = trajectory
        trajectories.append(trajectory)
        trajectory_states.append(states)

    # Normalize trajectories
    normalizer = get_normalizer(trajectories)
    normalized_trajectories = list()
    for trajectory in trajectories:
        normalized_trajectory = normalizer.transform(trajectory).tolist()
        normalized_trajectories.append(normalized_trajectory)
    trajectories = normalized_trajectories

    # Optimize world model
    remodel = hmm.GaussianHMM(n_components=4, covariance_type="full", n_iter=10000)
    X = np.concatenate(trajectories)
    lengths = [len(t) for t in trajectories]
    remodel.fit(X, lengths=lengths)

    # Serialize model
    # TODO

    # Plot losses
    # TODO
    plotting_data = [
        ("Forecasting loss (train)", []),  # loss array goes here
    ]
    for name, losses in plotting_data:
        losses_path = output_path/f"{name}.svg"
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.plot(losses)
        fig.savefig(losses_path)
        print(f"Wrote {losses_path}")

    # Plot model predictions in training data sub-trajectories
    n_given_step = 20
    prediction_steps = 40
    plot_prediction_n = min(10, len(trajectories))
    test_len = n_given_step + prediction_steps
    _random = random.Random(seed)
    test_trajectories = list()
    for _ in range(plot_prediction_n):
        trajectory = _random.choice(trajectories)
        indices = list(range(0, len(trajectory)-test_len))
        i = _random.choice(indices)
        sub_trajectory = trajectory[i:i+test_len]
        test_trajectories.append(sub_trajectory)
    for i, trajectory in enumerate(test_trajectories):
        # Plot predictions
        state_size = len(trajectory[0])
        y_given = trajectory[:n_given_step]
        hidden_states = remodel.predict(y_given)
        last_state = hidden_states[-1]
        model_output, _ = remodel.sample(
            n_samples=len(trajectory)-len(y_given),
            currstate=last_state,
        )
        y_pred = y_given + list(model_output)
        y_target = trajectory
        y_pred = torch.tensor(y_pred)
        y_target = torch.tensor(y_target)
        y_given = torch.tensor(y_given)
        plot_path = output_path/f"prediction_example_{i}.svg"
        my_dpi = 256  # Matplotlib's default DPI (100) is too low
        fig = Figure(figsize=(8, 6*state_size), dpi=my_dpi)
        FigureCanvas(fig)
        axs = fig.subplots(nrows=state_size)
        for i in range(state_size):
            axs[i].plot(y_pred[:, i].tolist(), label="prediction")
            axs[i].plot(y_target[:, i].tolist(), label="target")
            axs[i].plot(y_given[:, i].tolist(), label="given")
            axs[i].legend()
        fig.savefig(plot_path)
        print(f"Wrote {plot_path}")

    # Visualize top-level branching structure
    states = list()
    labels = list()
    for i, trajectory in enumerate(trajectories):
        t_labels = remodel.predict(trajectory)
        for j, t_label in enumerate(t_labels):
            state = trajectory_states[i][j]
            states.append(state)
            labels.append(t_label)
    cluster_path = output_path/"top-level-branch-clusters.png"
    plot_cluster_visualization(
        output_file=cluster_path,
        states=states,
        labels=labels,
    )
    print(f"Wrote {cluster_path}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Train a model of the quadcopter dynamics given a controller.',
    )
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--simulation-n',
        type=int,
        help='Number of simulations per test.',
        required=True,
    )
    parser.add_argument(
        '--output-path',
        type=Path,
        help='Number of simulations per test.',
        required=True,
    )
    args = parser.parse_args()
    args.output_path.mkdir()
    main(
        controller_path=args.controller_path,
        simulation_n=args.simulation_n,
        output_path=args.output_path,
        seed="asd",
    )
