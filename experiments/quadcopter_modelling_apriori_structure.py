"""Modelling the quadcopter dynamics with a pretrained controller.

The dynamics model has parametric if statements which split the
observation space into sub-models. The hope is that these sub-models
will correspond to the modes of the system. All parameters in the model
are jointly optimized.
"""
import argparse
from pathlib import Path
from control.neuroevolution import get_untrained_policy_network
from control.neuroevolution import get_random_initial_state
from control.neuroevolution import get_controller
from control.neuroevolution import get_controller_states
from control.neuroevolution import STEP_N
from control.clustering import get_state_vector
from world_modelling.neurosymbolic import interpreter
from world_modelling.neurosymbolic.dsl import serialize_program_instance
from world_modelling.neurosymbolic import dsl
from world_modelling.normalization import get_normalizer
from world_modelling.mlp import get_relu_mlp
from world_modelling.neurosymbolic.dsl import ProgramInstance
from world_modelling.neurosymbolic.optimizer import genetic_optimizer
from world_modelling.neurosymbolic.optimizer import gradient_ann_optimizer
from world_modelling.neurosymbolic.optimizer import genetic_constant_optimizer
from world_modelling.neurosymbolic.optimizer import hybrid_optimizer
from world_modelling.neurosymbolic.smooth_optimizer import gradient_smooth_optimizer
from world_modelling.types import Trajectory
from world_modelling.windows import get_trajectory_windows
from control.clustering import plot_cluster_visualization
from quadcopter_boxes.environment import State
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import torch
import random


def get_fixed_program_instance(
        input_size: int,
        output_size: int,
        mlp_hidden_sizes: list[int],
        bias: bool,
        verbose: bool,
        window_size: int,
        ):
    """Return a fixed program instance for the quadcopter environment."""
    # def program(observation, constants, ann_parameters):
    # if constants[0] < observation[0] and observation[0] < constants[1]:
    #     # if in wind region
    #     return neural_net(observation, ann_parameters[0])
    # else:
    #     # not in wind region
    #     return neural_net(observation, ann_parameters[1])
    program = dsl.Program(
        body=dsl.If(
            condition=dsl.And(
                left=dsl.LessThan(
                    left=dsl.ConstantsRead(
                        constants_index=0,
                    ),
                    right=dsl.LastObservationRead(
                        observation_index=0,
                    ),
                ),
                right=dsl.LessThan(
                    left=dsl.LastObservationRead(
                        observation_index=0,
                    ),
                    right=dsl.ConstantsRead(
                        constants_index=1,
                    ),
                ),
            ),
            then=dsl.Return(
                expression=dsl.AnnCall(ann_index=0),
            ),
            otherwise=dsl.Return(
                expression=dsl.AnnCall(ann_index=1),
            ),
        )
    )

    # This two numbers should correspond to the start and end of the wind
    # area (once they are optimized)
    constant_parameters = sorted([
        random.random(),
        random.random(),
    ])
    ann_modules = [
        get_relu_mlp(
            input_size=input_size,
            output_size=output_size,
            hidden_sizes=mlp_hidden_sizes,
            bias=bias,
            final_non_linear=False,
        ),
        get_relu_mlp(
            input_size=input_size,
            output_size=output_size,
            hidden_sizes=mlp_hidden_sizes,
            bias=bias,
            final_non_linear=False,
        ),
    ]

    # Optimize constant parameters and ANN modules
    program_instance = ProgramInstance(
        ast=program,
        ann_modules=ann_modules,
        constant_parameters=constant_parameters,
    )
    return program_instance


def get_top_level_branch_label(
        observation_window: Trajectory,
        program_instance: ProgramInstance,
        ) -> list[str]:
    """Return a label for each state, according to whether it is
    classified as true of false according to the top-level if statement
    in the program instance.

    It is assumed that the program instance AST is of the form
    `def program(obs): if COND then EXPR otherwise EXPR`."""
    condition = program_instance.ast.body.condition
    value = interpreter.evaluate_bool(
        node=condition,
        observation_window=observation_window,
        constant_parameters=program_instance.constant_parameters,
        ann_modules=program_instance.ann_modules,
        evaluate_bool=interpreter.evaluate_bool,
        evaluate_real=interpreter.evaluate_real,
        evaluate_vector=interpreter.evaluate_vector,
    )
    label = "1" if value else "0"
    return label


def main(
        controller_path: Path,
        simulation_n: int,
        output_path: Path,
        seed: str,
        ):
    """Train a model of the quadcopter dynamics given a controller."""
    # Open controller
    network = get_untrained_policy_network()
    network.load_state_dict(torch.load(controller_path))

    # Gather trajectories of maximum length with the controller
    trajectories = list()
    trajectory_states = list()
    while len(trajectories) < simulation_n:
        state = get_random_initial_state()
        controller = get_controller(network)
        states = get_controller_states(controller, state)
        # Ignore garbage trajectories
        if len(states) != STEP_N:
            print('skipping trajectory of incorrect length')
            continue
        # DEBUGGING: only consider the initial part of the trajectory
        #states = states[:60]
        # /DEBUGGING
        trajectory = [get_state_vector(x) for x in states]
        trajectory = trajectory
        trajectories.append(trajectory)
        trajectory_states.append(states)

    # Normalize trajectories
    normalizer = get_normalizer(trajectories)
    normalized_trajectories = list()
    for trajectory in trajectories:
        normalized_trajectory = normalizer.transform(trajectory).tolist()
        normalized_trajectories.append(normalized_trajectory)
    trajectories = normalized_trajectories

    # Optimize world model
    window_size = 5
    input_size = len(trajectories[0][0])*window_size
    output_size = len(trajectories[0][0])
    program_instance = get_fixed_program_instance(
        input_size=input_size,
        output_size=output_size,
        mlp_hidden_sizes=[128, 128],
        bias=True,
        verbose=True,
        window_size=window_size,
    )
    #result = genetic_optimizer(
    #    program_instance=program_instance,
    #    trajectories=trajectories,
    #    window_size=window_size,
    #    iter_n=3,
    #    population_size=2048,
    #    worker_n=32,
    #    mutation_stdev=0.1,
    #    tournament_size=4,
    #    verbose=True,
    #    batch_size=16,
    #)
    #result = gradient_ann_optimizer(
    #    program_instance=program_instance,
    #    trajectories=trajectories,
    #    window_size=window_size,
    #    iter_n=2048,
    #    learning_rate=0.01,
    #    verbose=True,
    #    batch_size=64,
    #)
    #result = genetic_constant_optimizer(
    #    program_instance=program_instance,
    #    trajectories=trajectories,
    #    window_size=window_size,
    #    iter_n=3,
    #    population_size=2048,
    #    worker_n=32,
    #    mutation_stdev=0.1,
    #    tournament_size=4,
    #    verbose=True,
    #    batch_size=16,
    #)
    #result = hybrid_optimizer(
    #    program_instance=program_instance,
    #    trajectories=trajectories,
    #    window_size=window_size,
    #    outer_iter_n=10,
    #    constants_iter_n=20,
    #    ann_modules_iter_n=1_000,
    #    learning_rate=0.001,
    #    population_size=128,
    #    worker_n=32,
    #    mutation_stdev=0.0005,
    #    tournament_size=2,
    #    verbose=True,
    #    batch_size=256,
    #)
    result = gradient_smooth_optimizer(
        program_instance=program_instance,
        trajectories=trajectories,
        window_size=window_size,
        iter_n=4096,
        learning_rate=0.0001,
        verbose=True,
        batch_size=1024,
    )

    # Serialize model
    model_dir = output_path/"model"
    model_dir.mkdir()
    serialize_program_instance(result.program_instance, model_dir)
    print(f"Wrote {model_dir}")

    # Plot losses
    plotting_data = [
        ("Forecasting loss (train)", result.losses.forecasting_losses),
    ]
    for name, losses in plotting_data:
        losses_path = output_path/f"{name}.svg"
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.plot(losses)
        fig.savefig(losses_path)
        print(f"Wrote {losses_path}")

    # Plot model predictions in training data sub-trajectories
    n_given_step = 20
    prediction_steps = 40
    plot_prediction_n = min(10, len(trajectories))
    test_len = n_given_step + prediction_steps
    _random = random.Random(seed)
    test_trajectories = list()
    for _ in range(plot_prediction_n):
        trajectory = _random.choice(trajectories)
        indices = list(range(0, len(trajectory)-test_len))
        i = _random.choice(indices)
        sub_trajectory = trajectory[i:i+test_len]
        test_trajectories.append(sub_trajectory)
    for i, trajectory in enumerate(test_trajectories):
        # Plot predictions
        state_size = len(trajectory[0])
        y_given = trajectory[:n_given_step]
        y_pred = y_given + interpreter.predict(
            trajectory=y_given,
            program_instance=result.program_instance,
            n_prediction=len(trajectory)-len(y_given),
            window_size=window_size,
        )
        y_target = trajectory
        y_pred = torch.tensor(y_pred)
        y_target = torch.tensor(y_target)
        y_given = torch.tensor(y_given)
        plot_path = output_path/f"prediction_example_{i}.svg"
        my_dpi = 256  # Matplotlib's default DPI (100) is too low
        fig = Figure(figsize=(8, 6*state_size), dpi=my_dpi)
        FigureCanvas(fig)
        axs = fig.subplots(nrows=state_size)
        for i in range(state_size):
            axs[i].plot(y_pred[:, i].tolist(), label="prediction")
            axs[i].plot(y_target[:, i].tolist(), label="target")
            axs[i].plot(y_given[:, i].tolist(), label="given")
            axs[i].legend()
        fig.savefig(plot_path)
        print(f"Wrote {plot_path}")

    # Visualize top-level branching structure
    states = list()
    labels = list()
    for i, trajectory in enumerate(trajectories):
        windows = get_trajectory_windows(trajectory, window_size)
        for j, window in enumerate(windows):
            label = get_top_level_branch_label(
                observation_window=window,
                program_instance=result.program_instance,
            )
            state = trajectory_states[i][j]
            states.append(state)
            labels.append(label)
    cluster_path = output_path/"top-level-branch-clusters.png"
    plot_cluster_visualization(
        output_file=cluster_path,
        states=states,
        labels=labels,
    )
    print(f"Wrote {cluster_path}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Train a model of the quadcopter dynamics given a controller.',
    )
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--simulation-n',
        type=int,
        help='Number of simulations per test.',
        required=True,
    )
    parser.add_argument(
        '--output-path',
        type=Path,
        help='Number of simulations per test.',
        required=True,
    )
    args = parser.parse_args()
    args.output_path.mkdir()
    main(
        controller_path=args.controller_path,
        simulation_n=args.simulation_n,
        output_path=args.output_path,
        seed="asd",
    )
