"""Modelling the quadcopter dynamics with a pretrained controller.

This script calls a STUN-inspired synthesis process but synthesizes a
neurosymbolic model.
"""
import argparse
from pathlib import Path
from control.neuroevolution import get_untrained_policy_network
from control.neuroevolution import get_random_initial_state
from control.neuroevolution import get_controller
from control.neuroevolution import get_controller_states
from control.neuroevolution import STEP_N
from control.clustering import get_state_vector
from world_modelling.neurosymbolic import interpreter
from world_modelling.neurosymbolic.dsl import serialize_program_instance
from world_modelling.neurosymbolic import dsl
from world_modelling.normalization import get_normalizer
from world_modelling.neurosymbolic.dsl import ProgramInstance
from world_modelling.neurosymbolic.synthesizer import get_optimized_world_model
from world_modelling.types import Trajectory
from world_modelling.windows import get_trajectory_windows
from control.clustering import plot_cluster_visualization
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import torch
import random


def get_top_level_branch_label(
        observation_window: Trajectory,
        program_instance: ProgramInstance,
        device: str,
        ) -> str:
    """Return a label for each state, according to whether it is
    classified as true of false according to the top-level if statement
    in the program instance.

    It is assumed that the program instance AST is of the form
    `def program(obs): if COND then EXPR otherwise EXPR`."""
    # If the program is not
    if not isinstance(program_instance.ast.body, dsl.If):
        #print("WARNING (get_top_level_branch_label): program is not an IfStatement")
        return "0"
    condition = program_instance.ast.body.condition
    value = interpreter.evaluate_bool(
        node=condition,
        observation_window=observation_window,
        constant_parameters=program_instance.constant_parameters,
        ann_modules=program_instance.ann_modules,
        sklearn_trees=program_instance.sklearn_trees,
        evaluate_bool=interpreter.evaluate_bool,
        evaluate_real=interpreter.evaluate_real,
        evaluate_vector=interpreter.evaluate_vector,
        device=device,
    )
    label = "1" if value else "0"
    return label


def main(
        controller_path: Path,
        simulation_n: int,
        output_path: Path,
        seed: str,
        ):
    """Train a model of the quadcopter dynamics given a controller."""
    _random = random.Random(seed)

    # Open controller
    network = get_untrained_policy_network()
    network.load_state_dict(torch.load(controller_path))

    # Gather trajectories of maximum length with the controller
    trajectories = list()
    raw_trajectory_states = list()
    while len(trajectories) < simulation_n:
        state = get_random_initial_state(str(_random.random()))
        controller = get_controller(network)
        states = get_controller_states(controller, state)

        # Ignore garbage trajectories
        if len(states) != STEP_N:
            print('skipping trajectory of incorrect length')
            continue

        trajectory = [get_state_vector(x) for x in states]
        trajectory = trajectory
        trajectories.append(trajectory)
        raw_trajectory_states.append(states)

    # Normalize trajectories
    normalizer = get_normalizer(trajectories, seed=_random.randint(0, 100000))
    normalized_trajectories = list()
    for trajectory in trajectories:
        normalized_trajectory = normalizer.transform(trajectory).tolist()
        normalized_trajectories.append(normalized_trajectory)
    trajectories = normalized_trajectories

    # Optimize world model
    window_size = 1
    debug_dir = output_path/"synthesis_debug"
    debug_dir.mkdir()
    result = get_optimized_world_model(
        trajectories=trajectories,
        mlp_hidden_sizes=[32, 32,],
        encoded_size=32,
        min_batch_size=16,
        seed=str(_random.random()),
        bias=True,
        iter_n=1024,
        learning_rate=0.01,
        verbose=True,
        batch_size=99999,
        window_size=window_size,
        depth_bound=0,
        device=args.device,
        max_prediction_error=0.3,
        use_logarithmic_objective=True,
        logarithm_constant=0.3,
        debug_dir=debug_dir,
    )

    # Serialize model
    model_dir = output_path/"model"
    model_dir.mkdir()
    serialize_program_instance(result.program_instance, model_dir)
    print(f"Wrote {model_dir}")

    # Plot losses
    plotting_data = [
        ("Forecasting loss (train)", result.losses.forecasting_losses),
    ]
    for name, losses in plotting_data:
        losses_path = output_path/f"{name}.svg"
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.plot(losses)
        fig.savefig(losses_path)
        print(f"Wrote {losses_path}")

    # Plot model predictions in training data sub-trajectories
    n_given_step = 20
    prediction_steps = 40
    plot_prediction_n = min(10, len(trajectories))
    test_len = n_given_step + prediction_steps
    _random = random.Random(seed)
    test_trajectories = list()
    for _ in range(plot_prediction_n):
        trajectory = _random.choice(trajectories)
        indices = list(range(0, len(trajectory)-test_len))
        i = _random.choice(indices)
        sub_trajectory = trajectory[i:i+test_len]
        test_trajectories.append(sub_trajectory)
    for i, trajectory in enumerate(test_trajectories):
        # Plot predictions
        state_size = len(trajectory[0])
        y_given = trajectory[:n_given_step]
        y_pred = y_given + interpreter.predict(
            trajectory=y_given,
            program_instance=result.program_instance,
            n_prediction=len(trajectory)-len(y_given),
            window_size=window_size,
            device=args.device,
        )
        y_target = trajectory
        y_pred = torch.tensor(y_pred)
        y_target = torch.tensor(y_target)
        y_given = torch.tensor(y_given)
        plot_path = output_path/f"prediction_example_{i}.svg"
        my_dpi = 256  # Matplotlib's default DPI (100) is too low
        fig = Figure(figsize=(8, 6*state_size), dpi=my_dpi)
        FigureCanvas(fig)
        axs = fig.subplots(nrows=state_size)
        for i in range(state_size):
            axs[i].plot(y_pred[:, i].tolist(), label="prediction")
            axs[i].plot(y_target[:, i].tolist(), label="target")
            axs[i].plot(y_given[:, i].tolist(), label="given")
            axs[i].legend()
        fig.savefig(plot_path)
        print(f"Wrote {plot_path}")

    # Visualize top-level branching structure
    states = list()
    labels = list()
    for i, trajectory in enumerate(trajectories):
        windows = get_trajectory_windows(trajectory, window_size)
        for j, window in enumerate(windows):
            label = get_top_level_branch_label(
                observation_window=window,
                program_instance=result.program_instance,
                device=args.device,
            )
            state = raw_trajectory_states[i][j]
            states.append(state)
            labels.append(label)
    cluster_path = output_path/"top-level-branch-clusters.png"
    plot_cluster_visualization(
        output_file=cluster_path,
        states=states,
        labels=labels,
    )
    print(f"Wrote {cluster_path}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Train a model of the dynamics given a controller.',
    )
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--simulation-n',
        type=int,
        help='Number of simulations per test.',
        required=True,
    )
    parser.add_argument(
        '--device',
        type=str,
        help='Device for training.',
        required=True,
    )
    parser.add_argument(
        '--output-path',
        type=Path,
        help='Number of simulations per test.',
        required=True,
    )
    args = parser.parse_args()
    args.output_path.mkdir()
    main(
        controller_path=args.controller_path,
        simulation_n=args.simulation_n,
        output_path=args.output_path,
        seed="asd",
    )
