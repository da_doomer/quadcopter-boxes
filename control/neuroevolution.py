"""Controller synthesis for the quadcopter cargo boxes task with
neuroevolution.

The controller is a waypoint following controller with an heuristic path.

The script was tested with the following parameters:
`python control/neuroevolution.py --output-dir $output_dir --worker-n 192 --pop-size 192 --iter-n 10000 --log-iter-freq 200`.

It converged around 4000 iterations.
"""
from quadcopter_boxes.environment import Controller
from quadcopter_boxes.environment import Action
from quadcopter_boxes.environment import ACTION_SIZE
from quadcopter_boxes.environment import simulate
from quadcopter_boxes.environment import State
from quadcopter_boxes.environment import BoxState
from quadcopter_boxes.environment import QuadcopterState
from quadcopter_boxes.environment import BOX_SIZE
from quadcopter_boxes.environment import SIMULATION_DT
from quadcopter_boxes.environment import DROPZONE_X1
from quadcopter_boxes.environment import DROPZONE_X0
from quadcopter_boxes.plotting import plot_animation
from evotorch.algorithms import Cosyne
from evotorch.neuroevolution import NEProblem
from evotorch.tools import device_of
from evotorch.logging import PandasLogger
from evotorch.logging import StdOutLogger
from dataclasses import dataclass
import concurrent.futures
from pathlib import Path
import torch
import math
import argparse
import random
import time
import statistics


BOX_N = 3

STEP_N = int(SIMULATION_DT**-1)*20  # 20 seconds


SEED = "hi"

def get_random_initial_state(seed: str) -> State:
    """Return a random initial state."""
    _random = random.Random(seed)
    initial_state = State(
        boxes=[
            BoxState(x=8.0+i*(BOX_SIZE[0]*1.5), y=BOX_SIZE[1]*2, vx=0.0, vy=0.0)
            for i in range(BOX_N)
        ],
        quadcopter=QuadcopterState(
            x=_random.uniform(5.0, 13.0),
            y=_random.uniform(0.3, 1.3),
            vx=_random.uniform(-0.5, 0.5),
            vy=_random.uniform(-0.5, 0.5),
            angle=0.0,
            angular_velocity=0.0,
            left_thrust=0.0,
            right_thrust=0.0,
        ),
        load_box_i=None,
        load_value=0.0,
    )
    return initial_state


WAYPOINT_RADIUS = 0.5


def get_waypoint(state: State) -> tuple[float, float]:
    """Return the current waypoint in world coordinates."""
    is_box_loaded = state.load_box_i is not None
    x = state.quadcopter.x
    y = state.quadcopter.y

    if not is_box_loaded:
        # Case: let's go pickup a box
        def sq_distance_to_quadcopter(box_state: BoxState) -> float:
            x = box_state.x
            y = box_state.y
            return ((state.quadcopter.x-x)**2+(state.quadcopter.y-y)**2)

        remaining_boxes = [
            box
            for box in state.boxes
            if box.x > DROPZONE_X1
        ]
        if len(remaining_boxes) == 0:
            remaining_boxes = state.boxes
        closest_box = min(remaining_boxes, key=sq_distance_to_quadcopter)
        undelivered_box_x = closest_box.x
        target_x = undelivered_box_x
        target_y = 0.8
    else:
        # Case: let's go deliver a box
        target_x = (DROPZONE_X1 - DROPZONE_X0)/2 + DROPZONE_X0
        target_y = 1.6

    # First move in y-axis
    if abs(target_y - y) > WAYPOINT_RADIUS:
        target_x = x

    # If target position is close, use it as waypoint
    rv = torch.tensor([target_x, target_y]) - torch.tensor([x, y])
    if rv.norm() <= WAYPOINT_RADIUS:
        return target_x, target_y

    # Otherwise, move a fixed amount towards the target positions
    rv = rv/rv.norm()*WAYPOINT_RADIUS*2
    intermediate = rv + torch.tensor([x, y])
    intermediate_x, _ = intermediate.tolist()
    return intermediate_x, target_y


Observation = tuple[
    float,
    float,
    float,
    float,
    float,
    float,
    float,
    float,
    float,
    float,
    float,
    float,
    float,
]
"""Observations are 11-dimensional vectors with the following entries:
0. quadcopter height (normalized)
1, 2. quadcopter x and y velocities (normalized)
3. quadcopter angle (normalized to [-1, 1])
4. quadcopter angular velocity
5, 6. position A x0, x1 coordinate (normalized, relative to quadcopter)
7, 8. closest box x and y coordinates (normalized, relative to quadcopter)
9, 10. closest box x and y velocities (normalized)
11. whether a box is loaded (0 or 1)
12, 13: waypoint x and y position

The closest box is the closest undelivered box if it exists, else it is an
arbitrary box. If there are no boxes in the environment, all box values will be
zero.
"""
OBSERVATION_SIZE = 13


def get_observation(state: State, waypoint: tuple[float, float]) -> Observation:
    """Return the observation induced by the given state."""
    # Transform positions to the same scale as velocities
    POSITION_SCALING = 2.0
    VELOCITY_SCALING = 1/0.5  # 0.3 units per second is already quite fast

    # Relative encoding of the waypoint and normalized to 2x waypoint
    # radius
    wx, wy = waypoint
    obs_wx = (wx - state.quadcopter.x)*POSITION_SCALING
    obs_wy = (wy - state.quadcopter.y)*POSITION_SCALING

    # Normalized angle and angular velocity
    angle_scaling = 1/math.pi
    obs_angle = state.quadcopter.angle*angle_scaling
    obs_angular_velocity = state.quadcopter.angular_velocity*angle_scaling

    # Relative dropzone coordinates
    obs_dropzone_x0 = POSITION_SCALING*(DROPZONE_X0 - state.quadcopter.x)
    obs_dropzone_x1 = POSITION_SCALING*(DROPZONE_X1 - state.quadcopter.x)

    # Relative quadcopter velocity
    obs_vx = state.quadcopter.vx*VELOCITY_SCALING
    obs_vy = state.quadcopter.vy*VELOCITY_SCALING

    # Thruster state
    left_thrust = state.quadcopter.left_thrust
    right_thrust = state.quadcopter.right_thrust

    # Relative "active" box position and velocity
    if len(state.boxes) == 0:
        obs_active_box_x = 0.0
        obs_active_box_y = 0.0
        obs_active_box_vx = 0.0
        obs_active_box_vy = 0.0
    else:
        if state.load_box_i is None:
            def sq_distance_to_quadcopter(box_state: BoxState) -> float:
                x = box_state.x
                y = box_state.y
                return ((state.quadcopter.x-x)**2+(state.quadcopter.y-y)**2)

            remaining_boxes = [
                box
                for box in state.boxes
                if box.x > DROPZONE_X1
            ]
            if len(remaining_boxes) == 0:
                remaining_boxes = state.boxes
            closest_box = min(remaining_boxes, key=sq_distance_to_quadcopter)
        else:
            closest_box = state.boxes[state.load_box_i]
        obs_active_box_x = POSITION_SCALING*(closest_box.x - state.quadcopter.x)
        obs_active_box_y = POSITION_SCALING*(closest_box.y - state.quadcopter.y)
        obs_active_box_vx = VELOCITY_SCALING*(closest_box.vx)
        obs_active_box_vy = VELOCITY_SCALING*(closest_box.vy)

    # Load state
    obs_load = -1.0 if state.load_box_i is None else 1.0

    return (
        obs_vx,
        obs_vy,
        obs_angle,
        obs_angular_velocity,
        obs_dropzone_x0,
        obs_dropzone_x1,
        obs_active_box_x,
        obs_active_box_y,
        obs_active_box_vx,
        obs_active_box_vy,
        obs_load,
        obs_wx,
        obs_wy,
    )


def get_loader_heuristic(state: State) -> float:
    """Simple heuristic to engage the loader when near a box that has to be
    loaded and engage it again when the box is ready to be offloaded."""
    x = state.quadcopter.x

    # Override loader value
    if state.load_box_i is not None and x <= DROPZONE_X1 and x >= DROPZONE_X0:
        # If we arrived at the dropzone, drop box if we are moving
        # very slowly
        vx = state.boxes[state.load_box_i].vx
        if abs(vx) <= 0.01:
            lv = 1.0
        else:
            lv = -1.0
    elif state.load_box_i is None and x > DROPZONE_X1:
        # If we are looking for a box, we try attach a box if
        # moving very slowly
        vx = state.quadcopter.vx
        if abs(vx) <= 0.01:
            lv = 1.0
        else:
            lv = -1.0
    else:
        # Otherwise, do not engage the loader
        lv = -1.0
    return lv


@dataclass
class StatefulWaypointController:
    """This class keeps track of the current waypoint and the waypoints
    for each time the controller was used. It also manually operates the
    loader using a heuristic."""
    current_waypoint: tuple[float, float] | None
    waypoints: list[tuple[float, float]]
    successful_waypoints: list[tuple[float, float]]
    network: torch.nn.Module

    def __call__(self, state: State) -> Action:
        # Check if we reached the current way point, in which case get a new
        # one
        if self.current_waypoint is None:
            self.current_waypoint = get_waypoint(state)
        x = state.quadcopter.x
        y = state.quadcopter.y
        rv = torch.tensor(self.current_waypoint) - torch.tensor([x, y])
        if rv.norm() <= WAYPOINT_RADIUS:
            # Record as successful only if waypoint is sufficiently far away
            # from previous successful waypoint
            if len(self.successful_waypoints) == 0:
                self.successful_waypoints.append(self.current_waypoint)
            else:
                w1 = torch.tensor(self.current_waypoint)
                w2 = torch.tensor(self.successful_waypoints[-1])
                rv2 = (w1-w2)
                if rv2.norm() > WAYPOINT_RADIUS:
                    self.successful_waypoints.append(self.current_waypoint)
            self.current_waypoint = get_waypoint(state)

        # Get and log current waypoint
        self.waypoints.append(self.current_waypoint)

        # Vectorize state
        observation = get_observation(state, self.current_waypoint)

        # Get action
        obs = torch.tensor(observation).to(device_of(self.network))
        obs = obs.clip(-1, 1)
        lt, rt, _ = self.network(obs).tolist()

        # Scale thrust to [0, 1]
        lt = (lt+1)/2
        rt = (rt+1)/2

        # Override loader with heuristic
        lv = get_loader_heuristic(state)

        return lt, rt, lv


def get_controller(network: torch.nn.Module) -> StatefulWaypointController:
    """Return the controller induced by the network."""
    controller = StatefulWaypointController(
        current_waypoint=None,
        waypoints=list(),
        network=network,
        successful_waypoints=list(),
    )
    return controller


def get_controller_states(
        controller: Controller,
        initial_state: State,
        ) -> list[State]:
    """Return the states induced by using the network as a controller."""
    states = simulate(initial_state, controller, STEP_N)
    return states


def get_waypoint_score(controller: StatefulWaypointController, initial_state: State) -> float:
    """Returns the score attained by the controller represented by the
    network on the given initial state."""
    get_controller_states(controller, initial_state)
    return len(controller.successful_waypoints)


SCORE_SAMPLE_N = 20


def get_expected_waypoint_score(network: torch.nn.Module) -> float:
    """Returns the expected score for the given network."""
    scores = list()
    for _ in range(SCORE_SAMPLE_N):
        initial_state = get_random_initial_state(str(random.random()))
        controller = get_controller(network)
        score = get_waypoint_score(controller, initial_state)
        scores.append(score)
    return statistics.mean(scores)


HIDDEN_SIZE = 64


def get_untrained_policy_network() -> torch.nn.Module:
    """Get a randomly-initialized Pytorch module that can act as a
    controller."""
    # The controller will be a way-point following controller. So the
    # observation size is the original observation plus two numbers
    # to encode the target waypoints position
    input_size = OBSERVATION_SIZE
    network = torch.nn.Sequential(
        torch.nn.Linear(input_size, HIDDEN_SIZE),
        torch.nn.ReLU(),
        torch.nn.Linear(HIDDEN_SIZE, ACTION_SIZE),
        torch.nn.Tanh(),
    )
    return network


def main():
    """Optimize and return a controller."""
    # Define search problem: find parameters for small MLP that maximize task
    # score
    parser = argparse.ArgumentParser(description='Optimize a controller.')
    parser.add_argument(
        '--output-dir',
        type=Path,
        help='Output directory to save results.',
        required=True,
    )
    parser.add_argument(
        '--worker-n',
        type=int,
        help='Number of workers to spawn.',
        required=True,
    )
    parser.add_argument(
        '--iter-n',
        type=int,
        help='Number of workers to spawn.',
        required=True,
    )
    parser.add_argument(
        '--pop-size',
        type=int,
        help='Number of workers to spawn.',
        required=True,
    )
    parser.add_argument(
        '--log-iter-freq',
        type=int,
        help='Log every this many optimization steps.',
        required=True,
    )
    args = parser.parse_args()
    output_dir = args.output_dir
    output_dir.mkdir()
    worker_n = args.worker_n
    iter_n = args.iter_n
    popsize = args.pop_size
    log_iter_freq = args.log_iter_freq

    # Initialize a controller and create the optimization problem
    # of optimizing its weight to maximize the score function
    network = get_untrained_policy_network()
    quadcopter_problem = NEProblem(
        objective_sense="max",
        network=network,
        network_eval_func=get_expected_waypoint_score,
        num_actors=worker_n,
    )

    # Log number of parameters
    n_parameters = sum(
        sum(network.state_dict()[param_tensor].size())
        for param_tensor in network.state_dict()
    )
    print(f"Controller parameter number: {n_parameters}")

    # Define search algorithm
    # Hyperparameters from:
    # https://docs.evotorch.ai/v0.4.1/examples/notebooks/Gym_Experiments_with_PGPE_and_CoSyNE/#training-with-cosyne
    searcher = Cosyne(
        quadcopter_problem,
        num_elites=1,
        popsize=popsize,
        tournament_size=4,
        mutation_stdev=0.3,
        mutation_probability=0.5,
        permute_all=True,
    )
    logger = PandasLogger(searcher)
    StdOutLogger(searcher)

    def get_trained_network():
        trained_network = quadcopter_problem.parameterize_net(
            searcher.status["pop_best"]
        )
        return trained_network

    # Define disk ologging
    start_t = time.time()
    def log(log_path: Path):
        # Visualize log results
        mean_eval_path = log_path/"mean_eval.svg"
        fig = logger.to_dataframe().mean_eval.plot().figure
        fig.savefig(mean_eval_path)
        fig.clf()
        print(f"Wrote {mean_eval_path}")

        # Get search algorithm result
        trained_network = get_trained_network()

        # Serialize model
        model_path = log_path/"model.pt"
        torch.save(trained_network.state_dict(), model_path)
        print(f"Wrote {model_path}")

        # Write elapsed time in seconds
        elapsed_seconds = time.time() - start_t
        elapsed_seconds_path = log_path/"elapsed_seconds.json"
        with open(elapsed_seconds_path, "wt") as fp:
            fp.write(str(elapsed_seconds))

        # Plot induced states (in parallel because it takes a while)
        with concurrent.futures.ProcessPoolExecutor(max_workers=worker_n) as executor:
            futures = list()
            for i in range(10):
                initial_state = get_random_initial_state(str(random.random()))
                controller = get_controller(trained_network)
                states = get_controller_states(controller, initial_state)
                waypoints = controller.waypoints
                states = states[:len(waypoints)]
                animation_file = log_path/f"neuroevolution_{i}.mp4"
                future = executor.submit(
                    plot_animation,
                    states=states,
                    output_file=animation_file,
                    margin=0.01,
                    FPS=int(SIMULATION_DT**-1),
                    waypoints=waypoints,
                )
                futures.append((animation_file, future))
            for animation_file, future in futures:
                future.result()
                print(f"Wrote {animation_file}")

    # Run search algorithm
    for step_i in range(iter_n):
        searcher.run(1)

        if step_i == iter_n -1 or step_i % log_iter_freq == 0:
            log_path = output_dir/f"step_{step_i+1}"
            log_path.mkdir()
            log(log_path)


if __name__ == "__main__":
    main()
