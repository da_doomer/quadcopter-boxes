"""Visualize a controller trained with `neuroevolution.py`."""


if __name__ == "__main__":
    # Define controller
    import argparse
    from pathlib import Path
    import torch
    parser = argparse.ArgumentParser(description='Optimize a controller.')
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the controller weights. None defaults to a random controller.',
        required=False,
    )
    from neuroevolution import HIDDEN_SIZE
    from neuroevolution import ACTION_SIZE
    from neuroevolution import OBSERVATION_SIZE
    from neuroevolution import get_controller
    input_size = OBSERVATION_SIZE
    network = torch.nn.Sequential(
        torch.nn.Linear(input_size, HIDDEN_SIZE),
        torch.nn.ReLU(),
        torch.nn.Linear(HIDDEN_SIZE, ACTION_SIZE),
        torch.nn.Tanh(),
    )
    args = parser.parse_args()
    if args.controller_path is not None:
        network.load_state_dict(torch.load(args.controller_path))
    controller = get_controller(network)

    # Run simulation for debugging
    from quadcopter_boxes.environment import State
    from quadcopter_boxes.environment import BoxState
    from quadcopter_boxes.environment import BOX_SIZE
    from quadcopter_boxes.environment import QuadcopterState
    from quadcopter_boxes.environment import simulate
    initial_state = State(
        boxes=[BoxState(x=8.0, y=BOX_SIZE[1]*2, vx=0.3, vy=0.0)],
        quadcopter=QuadcopterState(
            x=3.0,
            y=2.0,
            vx=0.0,
            vy=0.0,
            angle=0.0,
            angular_velocity=0.0,
            left_thrust=0.0,
            right_thrust=0.0,
        ),
        load_box_i=None,
        load_value=0.0,
    )
    states = simulate(initial_state, controller, 1280)

    from quadcopter_boxes.plotting import plot_animation
    from pathlib import Path

    animation_path = Path("environment.py.mp4")
    waypoints = controller.waypoints
    states = states[:len(waypoints)]
    plot_animation(states, animation_path, 0.1, 60, waypoints)
    print(f"Wrote {animation_path}")
