"""Experiment to train from scratch a controller with one policy for each
cluster.

This is similar to `control/clustering_neuroevolution.py` except that
we it does not require a pre-trained controller. It can be understood as
running that algorithm iteratively, starting with a random controller
as the pre-trained controller.
"""
from quadcopter_boxes.environment import State
from clustering_neuroevolution import optimize_modular_controller
from clustering_neuroevolution import get_state_module_i_function
from clustering_neuroevolution import get_controller
from clustering_neuroevolution import DecisionTreeIndexer
from neuroevolution import get_untrained_policy_network
from neuroevolution import get_controller_states, get_random_initial_state
from neuroevolution import get_controller as get_simple_controller
from pathlib import Path
import torch


def structured_optimization(
        initial_states: list[State],
        epoch_n: int,
        clustering_simulation_n: int,
        output_dir: Path,
        worker_n: int,
        pop_size: int,
        log_iter_freq: int,
        optimization_iter_n: int,
        min_cluster_size: int,
        log_video_n: int,
        verbose: bool,
        ) -> tuple[torch.nn.ModuleList, DecisionTreeIndexer]:
    states = initial_states
    network_modules = None
    tree = None

    for epoch_i in range(epoch_n):
        # Optimize modular controller
        epoch_output_dir = output_dir/f"epoch_{epoch_i}"
        epoch_output_dir.mkdir()
        network_modules, tree = optimize_modular_controller(
            initial_states=states,
            output_dir=epoch_output_dir,
            worker_n=worker_n,
            iter_n=optimization_iter_n,
            pop_size=pop_size,
            log_iter_freq=log_iter_freq,
            log_video_n=log_video_n,
            min_cluster_size=min_cluster_size,
            verbose=verbose,
        )

        get_state_module_i = get_state_module_i_function(tree)
        controller = get_controller(network_modules, get_state_module_i)

        # Gather new states with the new controller
        if epoch_i < epoch_n - 1:
            states = list[State]()
            for _ in range(clustering_simulation_n):
                state = get_random_initial_state()
                states.extend(get_controller_states(controller, state))

    assert network_modules is not None
    assert tree is not None
    return network_modules, tree


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Optimize a controller.')
    parser.add_argument(
        '--epoch-n',
        type=int,
        help='Number of epochs (i.e., outer-loop iterations).',
        required=True,
    )
    parser.add_argument(
        '--clustering-simulation-n',
        type=int,
        help='Number of simulations to characterize clusters.',
        required=True,
    )
    parser.add_argument(
        '--output-dir',
        type=Path,
        help='Output directory to save results.',
        required=True,
    )
    parser.add_argument(
        '--worker-n',
        type=int,
        help='Number of workers to spawn.',
        required=True,
    )
    parser.add_argument(
        '--optimization-iter-n',
        type=int,
        help='Number of workers to spawn.',
        required=True,
    )
    parser.add_argument(
        '--min-cluster-size',
        type=int,
        help='Minimum number of data points to form a cluster.',
        required=True,
    )
    parser.add_argument(
        '--pop-size',
        type=int,
        help='Population size in the genetic algorithm.',
        required=True,
    )
    parser.add_argument(
        '--log-iter-freq',
        type=int,
        help='Log every this many optimization steps.',
        required=True,
    )
    parser.add_argument(
        '--log-video-n',
        type=int,
        help='Log every this many optimization steps.',
        required=True,
    )
    args = parser.parse_args()
    output_dir = args.output_dir
    output_dir.mkdir()

    # Gather initial observations with random controller
    network = get_untrained_policy_network()
    controller = get_simple_controller(network)
    states = list[State]()
    for _ in range(args.clustering_simulation_n):
        state = get_random_initial_state()
        states.extend(get_controller_states(controller, state))

    # Run algorithm
    structured_optimization(
        epoch_n=args.epoch_n,
        clustering_simulation_n=args.clustering_simulation_n,
        initial_states=states,
        output_dir=args.output_dir,
        worker_n=args.worker_n,
        min_cluster_size=args.min_cluster_size,
        optimization_iter_n=args.optimization_iter_n,
        pop_size=args.pop_size,
        log_iter_freq=args.log_iter_freq,
        log_video_n=args.log_video_n,
        verbose=True,
    )


if __name__ == "__main__":
    main()
