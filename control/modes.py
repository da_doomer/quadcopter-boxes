"""Mode identification algorithm.

This algorithm takes a dataset of state transitions and identifies modes of
operation of the underlying dynamical system, with the assumption that
different modes are separated by discontinuities in the dynamics.
"""
from typing import Callable
from dataclasses import dataclass
from functools import cached_property
import statistics
import torch
from collections import defaultdict


Observation = tuple[float, ...]
Transition = tuple[Observation, Observation]
TransitionFunction = Callable[[Observation], Observation]
Synthesizer = Callable[
    [list[Transition]],
    TransitionFunction,
]


@dataclass
class Mode:
    """A mode is a set of transitions whose dynamics are well approximated by
    an model of the transition function induced by the given synthesizer."""
    transitions: tuple[Transition, ...]
    synthesizer: Synthesizer


    def extend(self, new_transitions: list[Transition]):
        """Add the transitions in the given list to the mode."""
        # Update current data
        self.transitions = self.transitions + tuple(new_transitions)

        # Invalidate cached transition function model
        del self.model


    @cached_property
    def model(self) -> TransitionFunction:
        """Synthesize a model of the transition function of the current
        data using the mode's synthesizer."""
        # Identify observations in the transition dataset
        X = list()
        Y = list()
        for s1, s2 in self.transitions:
            X.append(s1)
            Y.append(s2)

        # Assemble normalized transitions
        normalized_transitions = list()
        for x, y in zip(X, Y):
            normalized_transitions.append((x, y))

        model = self.synthesizer(normalized_transitions)
        return model


def get_mode_mean_error(
        mode: Mode,
        transitions: list[Transition],
        ) -> float:
    """Helper function to get the mean error of a mode with respect over
    some transitions."""
    errors = [
        (torch.tensor(mode.model(o1))-torch.tensor(o2)).norm().item()
        for o1, o2 in transitions
    ]
    mean_error = statistics.mean(errors)
    return mean_error


def get_modes(
        observations: list[Observation],
        transition_map: dict[int, int],
        synthesizer: Synthesizer,
        cluster_labels: list[int],
        initial_modes: list[Mode],
        max_error_factor: float,
        ) -> list[Mode]:
    """This algorithm takes a dataset of state transitions and identifies modes
    of operation of the underlying dynamical system, with the assumption that
    different modes are separated by discontinuities in the dynamics.

    The algorithm proceeds as follows. First, continuous regions of the
    dynamics are identified by a clustering algorithm. This separates
    observations corresponding to different modes of operation into different
    clusters. However, there might be clusters that actually correspond to the
    same mode (in the sense that they follow the same dynamics), even though
    they are separated by a discontinuity. The algorithm resolves this by
    synthesizing a model of the dynamics of a given mode and unifying all
    clusters that are well approximated by the model. Once possible clusters
    have been maximally unified, the result is a set of equivalence classes
    that correspond to the modes of operation of the dynamical system.

    The `initial_modes` parameter can be an empty list if the algorithm
    is being run for the first time. Then, modes can be updated with new
    data by passing the previous list of modes.

    Entries labeled with `-1` by the `get_clustering` method will be
    assigned a mode which cannot be unified with other modes.
    """
    cluster_names = list()
    clusters = list()
    for cluster_name1 in set(cluster_labels):
        cluster = [
            i
            for i, cluster_name2 in enumerate(cluster_labels)
            if cluster_name1 == cluster_name2
        ]
        cluster_names.append(cluster_name1)
        clusters.append(cluster)

    # Identify modes
    modes = list(initial_modes)
    cluster_i = 0
    cluster_map = defaultdict(list)
    for cluster_i in range(len(clusters)):
        # DEBUG: only add mode if cluster is not the "-1" cluster
        if cluster_names[cluster_i] == -1:
            continue

        # Choose an un-assigned cluster and identify associated transitions
        cluster = clusters[cluster_i]
        cluster_transitions: list[tuple[Observation, Observation]] = [
            (observations[i], observations[transition_map[i]])
            for i in cluster
            if i in transition_map.keys()
        ]

        # Check if the cluster belongs to an existing mode, only if
        # the cluster is not the "-1" cluster
        mode_i = None
        for i, mode in enumerate(modes):
            # Do not non-unifiable modes as targets for unification

            # A mode belongs to an existing mode if the dynamics of the mode
            # match the cluster up to an error epsilon
            mean_error = get_mode_mean_error(
                    mode,
                    cluster_transitions,
                    )
            if mean_error <= max_error_factor:
                mode_i = i
                break

        # Debug print target cluster
        target_mode = mode_i if mode_i is not None else len(modes)
        cluster_map[target_mode].append(cluster_i)

        # If the cluster belongs to an
        # existing mode, extend the mode.
        # Otherwise, create a new mode.
        if mode_i is not None:
            modes[mode_i].extend(cluster_transitions)
        else:
            mode = Mode(tuple(cluster_transitions), synthesizer)
            modes.append(mode)

    # Return the modes
    return modes
