from quadcopter_boxes.environment import State
from neuroevolution import get_controller_states, get_random_initial_state
from pathlib import Path
from clustering import cluster_vectors
from clustering import get_state_vector
from modes import get_modes
from modes import Observation
from modes import Transition
from modes import TransitionFunction
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import SGDRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.tree import DecisionTreeRegressor
from gplearn.genetic import SymbolicRegressor
from sklearn import preprocessing
import torch
import numpy as np
from neuroevolution import get_untrained_policy_network
from neuroevolution import get_controller
import argparse
from concurrent.futures import ProcessPoolExecutor


def synthesizer_linear(transitions: list[Transition]) -> TransitionFunction:
    """Return a function that approximates the given set of state transitions.
    The synthesizer function assumes that the observations in the transition
    dataset have been normalized.

    The returned function assumes that the input will be normalized."""
    # Turn transitions into a regression problem
    X = list()
    Y = list()
    for s1, s2 in transitions:
        X.append(s1)
        Y.append(s2)

    # Synthesize a linear function that approximates the transition function
    reg = LinearRegression().fit(X, Y)

    # The resulting program is simply the linear model
    def model(observation: Observation) -> Observation:
        # Predict vector
        predicted_vector = tuple[float, ...](reg.predict([observation])[0])
        return predicted_vector
    return model


def synthesizer_mlp(transitions: list[Transition]) -> TransitionFunction:
    """Return a function that approximates the given set of state transitions.
    The synthesizer function assumes that the observations in the transition
    dataset have been normalized.

    The returned function assumes that the input will be normalized."""
    # Turn transitions into a regression problem
    X = list()
    Y = list()
    for s1, s2 in transitions:
        X.append(s1)
        Y.append(s2)

    # Synthesize a linear function that approximates the transition function
    reg = MLPRegressor(
        max_iter=100000,
        solver="adam",
        hidden_layer_sizes=(32, 32),
        activation='tanh',
        learning_rate_init=0.01,
    ).fit(X, Y)

    # The resulting program is simply the linear model
    def model(observation: Observation) -> Observation:
        # Predict vector
        predicted_vector = tuple[float, ...](reg.predict([observation])[0])
        return predicted_vector
    return model


def synthesizer_tree(transitions: list[Transition]) -> TransitionFunction:
    """Return a function that approximates the given set of state transitions.
    The synthesizer function assumes that the observations in the transition
    dataset have been normalized.

    The returned function assumes that the input will be normalized."""
    # Turn transitions into a regression problem
    X = list()
    Y = list()
    for s1, s2 in transitions:
        X.append(s1)
        Y.append(s2)

    # Synthesize a linear function that approximates the transition function
    reg = DecisionTreeRegressor(max_depth=3).fit(X, Y)

    # The resulting program is simply the linear model
    def model(observation: Observation) -> Observation:
        # Predict vector
        predicted_vector = tuple[float, ...](reg.predict([observation])[0])
        return predicted_vector
    return model


def synthesizer_symbolic(transitions: list[Transition]) -> TransitionFunction:
    """Return a function that approximates the given set of state transitions.
    The synthesizer function assumes that the observations in the transition
    dataset have been normalized.

    The returned function assumes that the input will be normalized."""
    # Turn transitions into a regression problem
    X = list()
    Y = list()
    for s1, s2 in transitions:
        X.append(s1)
        Y.append(s2)

    # Synthesize a symbolic function that approximates the transition function
    feature_n = len(Y[0])
    regs = list()
    Y = np.array(Y)
    for i in range(feature_n):
        reg = SymbolicRegressor()
        reg.fit(X, Y[:, i])
        regs.append(reg)

    # The resulting program is simply the linear model
    def model(observation: Observation) -> Observation:
        # Predict vector
        predicted_vector = list[float]()
        for reg in regs:
            y = reg.predict([observation])[0]
            predicted_vector.append(y)
        return tuple(predicted_vector)
    return model


def synthesizer_linear_sgd(transitions: list[Transition]) -> TransitionFunction:
    """Return a function that approximates the given set of state transitions.
    The synthesizer function assumes that the observations in the transition
    dataset have been normalized.

    The returned function assumes that the input will be normalized."""
    # Turn transitions into a regression problem
    X = list()
    Y = list()
    for s1, s2 in transitions:
        X.append(s1)
        Y.append(s2)

    # Synthesize a symbolic function that approximates the transition function
    feature_n = len(Y[0])
    regs = list()
    Y = np.array(Y)
    for i in range(feature_n):
        reg = SGDRegressor()
        reg.fit(X, Y[:, i])
        regs.append(reg)

    # The resulting program is simply the linear model
    def model(observation: Observation) -> Observation:
        # Predict vector
        predicted_vector = list[float]()
        for reg in regs:
            y = reg.predict([observation])[0]
            predicted_vector.append(y)
        return tuple(predicted_vector)
    return model


def get_covered_regions(states: list[State]) -> set[int]:
    """A label "covers" a region if at least 10% of its associated states fall
    inside the region."""
    regions = list()
    for state in states:
        x = state.quadcopter.x
        carrying_load = state.load_box_i is not None
        if x < 4 and carrying_load:
            region = 0
        elif x < 4 and not carrying_load:
            region = 1
        elif x > 6 and carrying_load:
            region = 2
        elif x > 6 and not carrying_load:
            region = 3
        elif 4 < x < 6 and carrying_load:
            region = 4
        else:  # 4 < x < 6 and not carrying_load:
            region = 5
        regions.append(region)

    covered_regions = set()
    for region in set(regions):
        coverage = len([r for r in regions if r == region])/len(regions)
        if coverage >= 0.10:
            covered_regions.add(region)
    return covered_regions

def test_clustering(
        observations: list[Observation],
        state_map: dict[Observation, State],
        min_cluster_size: int,
        ) -> bool:
    """A clustering is correct if and only if each label only spans
    a single region AND every region has at least one corresponding
    cluster."""
    cluster_labels = cluster_vectors(observations, min_cluster_size)
    covered_regions = set()
    for label in set(cluster_labels):
        if label == -1:
            continue
        label_observations = [
            state_map[observation]
            for observation, o_label in zip(observations, cluster_labels)
            if o_label == label
        ]
        label_regions = get_covered_regions(
            label_observations,
        )
        if len(label_regions) != 1:
            return False
        covered_regions.add(list(label_regions)[0])
    if len(covered_regions) != 6:
        return False
    return True


def get_mode_labels(
        observations: list[Observation],
        transition_map: dict[int, int],
        min_cluster_size: int,
        max_error_factor: float,
        ) -> list[int]:
    # Run mode identification algorithm
    cluster_labels = cluster_vectors(observations, min_cluster_size)
    modes = get_modes(
        observations=observations,
        transition_map=transition_map,
        synthesizer=synthesizer_mlp,
        #synthesizer=synthesizer_linear_sgd,
        cluster_labels=cluster_labels,
        initial_modes=list(),
        max_error_factor=max_error_factor,
    )

    # Identify states in each mode
    mode_labels = list()
    for observation in observations:
        current_modes = list()
        for i, mode in enumerate(modes):
            for (v1, v2) in mode.transitions:
                for v in [v1, v2]:
                    if v == observation:
                        current_modes.append(i)

        if len(current_modes) == 0:
            # I made a change to mode unification to ignore outlier cluster,
            # so not all observations are assigned to a mode
            mode_labels.append(-1)
        else:
            mode_labels.append(current_modes[0])

    return mode_labels


def test_mode_identification(
        observations: list[Observation],
        transition_map: dict[int, int],
        state_map: dict[Observation, State],
        min_cluster_size: int,
        max_error_factor: float,
        ) -> bool:
    """A mode identification is correct if and only if there are four labels,
    with labels spanning exactly each one of the following label combinations:
    (1, 3), (2, 4), (5) and (6)."""
    mode_labels = get_mode_labels(
        observations,
        transition_map,
        min_cluster_size,
        max_error_factor,
    )
    covered_regions = list()
    for label in set(mode_labels):
        if label == -1:
            continue
        label_observations = [
            state_map[observation]
            for observation, o_label in zip(observations, mode_labels)
            if o_label == label
        ]
        label_regions = get_covered_regions(
            label_observations,
        )
        covered_regions.append(sorted(label_regions))
    print(covered_regions)
    correct_regions = [
        sorted({0, 2}),
        sorted({1, 3}),
        sorted({4,}),
        sorted({5,}),
    ]
    return sorted(covered_regions) == sorted(correct_regions)


def test(controller_path: Path, simulation_n: int, min_cluster_size: int, max_error_factor: float):
    network = get_untrained_policy_network()
    network.load_state_dict(torch.load(controller_path))

    # Gather transitions with the controller
    transitions = list[tuple[State, State]]()
    for _ in range(simulation_n):
        state = get_random_initial_state()
        controller = get_controller(network)
        states = get_controller_states(controller, state)
        for i, s1 in enumerate(states[:-1]):
            s2 = states[i+1]
            transitions.append((s1, s2))

    # Create data-structure that keeps track of transitions as indices
    observations = list[Observation]()
    transition_map = dict[int, int]()
    observation_map = dict[Observation, int]()
    state_map = dict[Observation, State]()
    for (s1, s2) in transitions:
        v1 = get_state_vector(s1)
        v2 = get_state_vector(s2)
        if v1 not in observation_map.keys():
            observation_map[v1] = len(observations)
            observations.append(v1)
        if v2 not in observation_map.keys():
            observation_map[v2] = len(observations)
            observations.append(v2)
        i = observation_map[v1]
        j = observation_map[v2]
        transition_map[i] = j
        state_map[v1] = s1
        state_map[v2] = s2

    # Compute statistics to normalize vectors (using s1 only to avoid
    # duplicate vectors)
    scaler = preprocessing.MinMaxScaler(feature_range=(-1, 1)).fit(observations)

    # Normalize observations
    normalized_observations = list()
    normalized_map = dict[Observation, State]()
    for observation in observations:
        nv = tuple(scaler.transform([observation])[0])
        normalized_observations.append(nv)
        normalized_map[nv] = state_map[observation]

    # Test algorithms
    result_clustering = test_clustering(
        normalized_observations, normalized_map, min_cluster_size,
    )
    result_modes = test_mode_identification(
        normalized_observations, transition_map, normalized_map, min_cluster_size, max_error_factor
    )
    return result_clustering, result_modes


if __name__ == "__main__":
    # Define controller
    parser = argparse.ArgumentParser(description='Visualize output of mode identification algorithm.')
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--simulation-n',
        type=int,
        help='Number of simulations per test.',
        required=True,
    )
    parser.add_argument(
        '--test-n',
        type=int,
        help='Number of tests.',
        required=True,
    )
    parser.add_argument(
        '--worker-n',
        type=int,
        help='Number of tests.',
        required=True,
    )
    args = parser.parse_args()

    futures = list()
    min_cluster_size = 120
    max_error_factor = 0.8
    simulation_n = args.simulation_n
    with ProcessPoolExecutor(max_workers=args.worker_n) as pool:
        for _ in range(args.test_n):
            future = pool.submit(
                test,
                args.controller_path,
                simulation_n,
                min_cluster_size,
                max_error_factor,
            )
            futures.append(future)

    results_clustering = list()
    results_modes = list()
    for future in futures:
        result_clustering, result_modes = future.result()
        results_clustering.append(result_clustering)
        results_modes.append(result_modes)

    accuracy_clustering = sum(results_clustering)/len(results_clustering)
    accuracy_modes = sum(results_modes)/len(results_modes)

    conditional_mode_accuracies = list()
    for result_clustering, result_modes in zip(results_clustering, results_modes):
        if result_clustering:
            conditional_mode_accuracies.append(result_modes)
    conditional_mode_accuracy = sum(conditional_mode_accuracies)/len(conditional_mode_accuracies)

    print(f"Accuracy clustering: {round(accuracy_clustering*100, 2)}%")
    print(f"Accuracy modes: {round(accuracy_modes*100, 2)}%")
    print(f"Conditional accuracy modes: {round(conditional_mode_accuracy*100, 2)}%")
