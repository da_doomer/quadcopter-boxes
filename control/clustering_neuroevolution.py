"""Experiment using clusters to train a controller with one policy for each
cluster."""
from quadcopter_boxes.environment import State
from quadcopter_boxes.environment import Action
from quadcopter_boxes.environment import Controller
from quadcopter_boxes.environment import simulate
from quadcopter_boxes.environment import SIMULATION_DT
from quadcopter_boxes.plotting import plot_animation
from neuroevolution import get_controller_states, get_random_initial_state
from neuroevolution import get_waypoint
from neuroevolution import WAYPOINT_RADIUS
from neuroevolution import get_observation
from neuroevolution import STEP_N
from neuroevolution import SCORE_SAMPLE_N
from neuroevolution import get_controller as get_simple_controller
from neuroevolution import get_loader_heuristic
from neuroevolution import get_untrained_policy_network
from clustering import get_state_vector
from clustering import plot_decision_tree
from clustering import cluster_vectors
from clustering import plot_cluster_visualization
from evotorch.tools import device_of
from evotorch.neuroevolution import NEProblem
from evotorch.logging import PandasLogger
from evotorch.logging import StdOutLogger
from evotorch.algorithms import Cosyne
from dataclasses import dataclass
from typing import Callable
from pathlib import Path
import sklearn.tree
import torch
import statistics
import time
import concurrent.futures
import torch
import joblib
import gc


class ModularNEProblem(NEProblem):
    def __init__(self, modular_controller: torch.nn.ModuleList, decision_tree_path: Path, min_label: int, worker_n: int):
        self.decision_tree_path = decision_tree_path
        self.min_label = min_label
        super().__init__(
            objective_sense="max",
            network=modular_controller,
            num_actors=worker_n,
        )

    def _evaluate_network(self, net: torch.nn.ModuleList) -> float:
        score = get_expected_waypoint_score(
            net,
            self.decision_tree_path,
            self.min_label,
        )
        return score


@dataclass
class ModularStatefulWaypointController:
    """This class keeps track of the current waypoint and the waypoints
    for each time the controller was used. It also manually operates the
    loader using a heuristic.

    It engages different modules according to the given classifier function.
    """
    current_waypoint: tuple[float, float] | None
    waypoints: list[tuple[float, float]]
    successful_waypoints: list[tuple[float, float]]
    modules: torch.nn.ModuleList
    get_state_module_i: Callable[[State], int]

    def __call__(self, state: State) -> Action:
        # Check if we reached the current way point, in which case get a new
        # one
        if self.current_waypoint is None:
            self.current_waypoint = get_waypoint(state)
        x = state.quadcopter.x
        y = state.quadcopter.y
        rv = torch.tensor(self.current_waypoint) - torch.tensor([x, y])
        if rv.norm() <= WAYPOINT_RADIUS:
            # Record as successful only if waypoint is sufficiently far away
            # from previous successful waypoint
            if len(self.successful_waypoints) == 0:
                self.successful_waypoints.append(self.current_waypoint)
            else:
                w1 = torch.tensor(self.current_waypoint)
                w2 = torch.tensor(self.successful_waypoints[-1])
                rv2 = (w1-w2)
                if rv2.norm() > WAYPOINT_RADIUS:
                    self.successful_waypoints.append(self.current_waypoint)
            self.current_waypoint = get_waypoint(state)

        # Get and log current waypoint
        self.waypoints.append(self.current_waypoint)

        # Vectorize state
        observation = get_observation(state, self.current_waypoint)

        # Get action
        obs = torch.tensor(observation).to(device_of(self.modules))
        obs = obs.clip(-1, 1)
        module_i = self.get_state_module_i(state)
        if module_i >= len(self.modules):
            # If in doubt, default to module 0
            module_i = 0
        lt, rt, _ = self.modules[module_i](obs).tolist()

        # Scale thrust to [0, 1]
        lt = (lt+1)/2
        rt = (rt+1)/2

        # Override loader with heuristic
        lv = get_loader_heuristic(state)

        return lt, rt, lv


def get_controller(
        modules: torch.nn.ModuleList,
        get_state_module_i: Callable[[State], int],
        ) -> ModularStatefulWaypointController:
    """Return the controller induced by the network."""
    controller = ModularStatefulWaypointController(
        current_waypoint=None,
        waypoints=list(),
        modules=modules,
        successful_waypoints=list(),
        get_state_module_i=get_state_module_i,
    )
    return controller


def get_controller_states(
        controller: Controller,
        initial_state: State,
        ) -> list[State]:
    """Return the states induced by using the network as a controller."""
    states = simulate(initial_state, controller, STEP_N)
    return states


def get_waypoint_score(controller: ModularStatefulWaypointController, initial_state: State) -> float:
    """Returns the score attained by the controller represented by the
    network on the given initial state."""
    get_controller_states(controller, initial_state)
    return len(controller.successful_waypoints)


@dataclass
class DecisionTreeIndexer:
    """Extremely simple wrapper that keeps track of the minimum index
    given by a decision tree, which is often -1 in sklearn."""
    min_label: int
    clf: sklearn.tree.DecisionTreeClassifier



def get_state_module_i_function(tree: DecisionTreeIndexer) -> Callable[[State], int]:
    """Get the classification function stored in the given path."""
    clf = tree.clf
    def get_state_module_i(state: State) -> int:
        x = get_state_vector(state)
        label = clf.predict([x])[0]
        # There might be a "-1" cluster, so we account for that
        return label-tree.min_label
    return get_state_module_i


def get_expected_waypoint_score(
        modules: torch.nn.ModuleList,
        decision_tree_path: Path,
        min_label: int,
        ) -> float:
    """Returns the expected score for the given network."""
    # Turn decision tree into classifier
    clf = joblib.load(decision_tree_path)
    tree = DecisionTreeIndexer(min_label=min_label, clf=clf)
    get_state_module_i = get_state_module_i_function(tree)

    scores = list()
    for _ in range(SCORE_SAMPLE_N):
        initial_state = get_random_initial_state()
        controller = get_controller(modules, get_state_module_i)
        score = get_waypoint_score(controller, initial_state)
        scores.append(score)
    return statistics.mean(scores)


def optimize_modular_controller(
        initial_states: list[State],
        output_dir: Path,
        worker_n: int,
        iter_n: int,
        pop_size: int,
        log_iter_freq: int | None,
        log_video_n: int | None,
        min_cluster_size: int,
        verbose: bool,
        ) -> tuple[torch.nn.ModuleList, DecisionTreeIndexer]:
    """Returns the weights the modules and the decision tree classifier
    that define a modular controller."""
    # Cluster states
    vectors = [get_state_vector(s) for s in initial_states]
    labels = cluster_vectors(vectors, min_cluster_size=min_cluster_size)
    cluster_n = len(set(labels))
    if verbose: print(f"Cluster number: {cluster_n}")

    # Plot clusters
    output_file = output_dir/"clusters.png"
    str_labels = [f"Cluster {i}" for i in labels]
    plot_cluster_visualization(output_file, initial_states, str_labels)
    if verbose: print(f"Wrote {output_file}")

    # Characterize clusters with a decision tree
    X = [get_state_vector(x) for x in initial_states]
    Y = labels
    clf = sklearn.tree.DecisionTreeClassifier().fit(X, Y)

    # Plot tree
    output_file = output_dir/f"tree.svg"
    plot_decision_tree(clf, output_file, str_labels, initial_states)
    if verbose: print(f"Wrote {output_file}")

    # Serialize tree
    decision_tree_path = output_dir/"tree.joblib"
    joblib.dump(clf, decision_tree_path)

    # Garbage collect before creating parallelized problem
    del X
    del initial_states
    del clf
    del Y
    gc.collect()

    # Create modular controller
    modules = [
        get_untrained_policy_network()
        for _ in range(cluster_n)
    ]
    for module in modules:
        module.load_state_dict(get_untrained_policy_network().state_dict())
    modular_controller = torch.nn.ModuleList(modules=modules)

    # Initialize a controller and create the optimization problem
    # of optimizing its weight to maximize the score function
    min_label = min(labels)
    quadcopter_problem = ModularNEProblem(
        modular_controller=modular_controller,
        decision_tree_path=decision_tree_path,
        min_label=min_label,
        worker_n=worker_n,
    )

    # Log number of parameters
    n_parameters = sum(
        sum(modular_controller.state_dict()[param_tensor].size())
        for param_tensor in modular_controller.state_dict()
    )
    if verbose: print(f"Controller parameter number: {n_parameters}")

    # Define search algorithm
    # Hyperparameters from:
    # https://docs.evotorch.ai/v0.4.1/examples/notebooks/Gym_Experiments_with_PGPE_and_CoSyNE/#training-with-cosyne
    searcher = Cosyne(
        quadcopter_problem,
        num_elites=1,
        popsize=pop_size,
        tournament_size=4,
        mutation_stdev=0.3,
        mutation_probability=0.5,
        permute_all=True,
    )
    logger = PandasLogger(searcher)
    StdOutLogger(searcher)

    def get_trained_network() -> torch.nn.ModuleList:
        trained_network = quadcopter_problem.parameterize_net(
            searcher.status["pop_best"]
        )
        # (False type error caused by incorrect type annotation in evotorch)
        return trained_network

    # Define disk ologging
    start_t = time.time()
    def log(log_path: Path):
        # Visualize log results
        mean_eval_path = log_path/"mean_eval.png"
        fig = logger.to_dataframe().mean_eval.plot().figure
        fig.savefig(mean_eval_path)
        fig.clf()
        if verbose: print(f"Wrote {mean_eval_path}")

        # Serialize log results
        json_path = log_path/f"logger.json"
        logger.to_dataframe().to_json(json_path)
        if verbose: print(f"Wrote {json_path}")

        # Get search algorithm result
        trained_network = get_trained_network()

        # Serialize model
        model_path = log_path/"structured_model.pt"
        torch.save(trained_network.state_dict(), model_path)
        if verbose: print(f"Wrote {model_path}")

        # Write elapsed time in seconds
        elapsed_seconds = time.time() - start_t
        elapsed_seconds_path = log_path/"elapsed_seconds.json"
        with open(elapsed_seconds_path, "wt") as fp:
            fp.write(str(elapsed_seconds))

        # Plot induced states (in parallel because it takes a while)
        if log_video_n is not None:
            with concurrent.futures.ProcessPoolExecutor(max_workers=worker_n) as executor:
                futures = list()
                for i in range(log_video_n):
                    initial_state = get_random_initial_state()
                    clf = joblib.load(decision_tree_path)
                    tree = DecisionTreeIndexer(clf=clf, min_label=min_label)
                    get_state_module_i = get_state_module_i_function(tree)
                    controller = get_controller(trained_network, get_state_module_i)
                    states = get_controller_states(controller, initial_state)
                    waypoints = controller.waypoints
                    states = states[:len(waypoints)]
                    video_file = log_path/f"neuroevolution_{i}.mp4"
                    future = executor.submit(
                        plot_animation,
                        states=states,
                        output_file=video_file,
                        margin=0.01,
                        FPS=int(SIMULATION_DT**-1),
                        waypoints=waypoints,
                    )
                    futures.append((video_file, future))
                for video_file, future in futures:
                    future.result()
                    if verbose: print(f"Wrote {video_file}")

    # Run search algorithm
    for step_i in range(iter_n):
        searcher.run(1)

        if log_iter_freq is not None and (step_i == iter_n - 1 or step_i % log_iter_freq == 0):
            start_t = time.time()
            log_path = output_dir/f"step_{step_i+1}"
            log_path.mkdir()
            log(log_path)
            print(f"Logging took {round(time.time()-start_t, 2)}s")

    clf = joblib.load(decision_tree_path)
    tree = DecisionTreeIndexer(clf=clf, min_label=min_label)
    return get_trained_network(), tree


def main():
    import argparse
    parser = argparse.ArgumentParser(description='Optimize a controller.')
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--clustering-simulation-n',
        type=int,
        help='Number of simulations to characterize clusters.',
        required=True,
    )
    parser.add_argument(
        '--output-dir',
        type=Path,
        help='Output directory to save results.',
        required=True,
    )
    parser.add_argument(
        '--worker-n',
        type=int,
        help='Number of workers to spawn.',
        required=True,
    )
    parser.add_argument(
        '--iter-n',
        type=int,
        help='Number of workers to spawn.',
        required=True,
    )
    parser.add_argument(
        '--min-cluster-size',
        type=int,
        help='Minimum number of data points to form a cluster.',
        required=True,
    )
    parser.add_argument(
        '--pop-size',
        type=int,
        help='Population size in the genetic algorithm.',
        required=True,
    )
    parser.add_argument(
        '--log-iter-freq',
        type=int,
        help='Log every this many optimization steps.',
        required=True,
    )
    parser.add_argument(
        '--log-video-n',
        type=int,
        help='Log every this many optimization steps.',
        required=True,
    )
    args = parser.parse_args()
    output_dir = args.output_dir
    output_dir.mkdir()

    # Load initial controller
    network = get_untrained_policy_network()
    network.load_state_dict(torch.load(args.controller_path))

    # Gather states with the controller
    states = list[State]()
    for _ in range(args.clustering_simulation_n):
        state = get_random_initial_state()
        controller = get_simple_controller(network)
        states.extend(get_controller_states(controller, state))

    optimize_modular_controller(
        initial_states=states,
        output_dir=args.output_dir,
        worker_n=args.worker_n,
        iter_n=args.iter_n,
        min_cluster_size=args.min_cluster_size,
        pop_size=args.pop_size,
        log_iter_freq=args.log_iter_freq,
        log_video_n=args.log_video_n,
        verbose=True,
    )

if __name__ == "__main__":
    main()
