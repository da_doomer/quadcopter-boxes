"""Visualize output of mode identification algorithm with two plots that show
input clusters and the resulting modes."""
from quadcopter_boxes.environment import State
from neuroevolution import get_controller_states, get_random_initial_state
from sklearn import tree
from pathlib import Path
from clustering import cluster_vectors
from clustering import plot_cluster_visualization
from clustering import get_state_vector
from clustering import plot_decision_tree
from modes import get_modes
from modes import Observation
from test_modes import synthesizer_mlp
from sklearn import preprocessing
import torch
import argparse
from pathlib import Path
import torch
from neuroevolution import get_untrained_policy_network
from neuroevolution import get_controller


def inspect_modes(
        controller_path: Path,
        simulation_n: int,
        min_cluster_size: int,
        output_dir: Path,
        ):
    network = get_untrained_policy_network()
    if controller_path is not None:
        network.load_state_dict(torch.load(controller_path))

    # Gather transitions with the controller
    transitions = list[tuple[State, State]]()
    for _ in range(simulation_n):
        state = get_random_initial_state()
        controller = get_controller(network)
        states = get_controller_states(controller, state)
        for i, s1 in enumerate(states[:-1]):
            s2 = states[i+1]
            transitions.append((s1, s2))

    # Create data-structure that keeps track of transitions as indices
    observations = list[Observation]()
    transition_map = dict[int, int]()
    observation_map = dict[Observation, int]()
    state_map = dict[Observation, State]()
    for (s1, s2) in transitions:
        v1 = get_state_vector(s1)
        v2 = get_state_vector(s2)
        if v1 not in observation_map.keys():
            observation_map[v1] = len(observations)
            observations.append(v1)
        if v2 not in observation_map.keys():
            observation_map[v2] = len(observations)
            observations.append(v2)
        i = observation_map[v1]
        j = observation_map[v2]
        transition_map[i] = j
        state_map[v1] = s1
        state_map[v2] = s2

    # Compute statistics to normalize vectors (using s1 only to avoid
    # duplicate vectors)
    scaler = preprocessing.MinMaxScaler(feature_range=(-1, 1)).fit(observations)

    # Normalize observations
    normalized_observations = list()
    normalized_map = dict[Observation, int]()
    for observation in observations:
        nv = tuple(scaler.transform([observation])[0])
        normalized_observations.append(nv)
        normalized_map[nv] = observation_map[observation]

    # Cluster states
    cluster_labels = cluster_vectors(normalized_observations, min_cluster_size)
    print(f"Cluster #: {len(set(cluster_labels))}")
    output_file = output_dir/"clusters.png"
    str_mode_labels = [f"Cluster {i}" for i in cluster_labels]
    states = [state_map[v] for v in observations]
    plot_cluster_visualization(output_file, states, str_mode_labels)
    print(f"Wrote {output_file}")

    # Run mode identification algorithm
    modes = get_modes(
        observations=normalized_observations,
        transition_map=transition_map,
        synthesizer=synthesizer_mlp,
        cluster_labels=cluster_labels,
        initial_modes=list(),
        max_error_factor=0.5,
    )

    print(f"Mode #: {len(modes)}")

    # Identify states in each mode
    mode_labels = list()
    states = list()
    for i, mode in enumerate(modes):
        for (v1, _) in mode.transitions:
            s1 = state_map[observations[normalized_map[v1]]]
            states.append(s1)
            mode_labels.append(i)

    # Plot clusters
    output_file = output_dir/"modes.png"
    str_mode_labels = [f"Mode {i}" for i in mode_labels]
    plot_cluster_visualization(output_file, states, str_mode_labels)
    print(f"Wrote {output_file}")

    # Characterize clusters with a decision tree
    X = [get_state_vector(x) for x in states]
    Y = mode_labels
    clf = tree.DecisionTreeClassifier().fit(X, Y)

    # Plot tree
    output_file = output_dir/f"tree.svg"
    plot_decision_tree(clf, output_file, str_mode_labels, states)
    print(f"Wrote {output_file}")


if __name__ == "__main__":
    # Define controller
    parser = argparse.ArgumentParser(description='Visualize output of mode identification algorithm.')
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--simulation-n',
        type=int,
        help='Number of simulations.',
        required=True,
    )
    args = parser.parse_args()
    output_dir = Path("inspect_modes")
    output_dir.mkdir()
    inspect_modes(args.controller_path, args.simulation, 80, output_dir)
