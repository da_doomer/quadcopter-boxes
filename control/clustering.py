"""Experiment clustering observations. This script has been tested with a
controller trained with the neuroevolution module and `--simulation-n 100`."""
from quadcopter_boxes.environment import State, get_wind_velocity
from control.neuroevolution import get_controller_states, get_random_initial_state
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from sklearn.cluster import HDBSCAN
from sklearn import tree
from sklearn import preprocessing
import matplotlib.colors
import matplotlib.pyplot
from pathlib import Path


def get_vectorized_state_with_labels(state: State) -> tuple[tuple[float, str], ...]:
    """Return a vectorization of the given state."""
    x, y = state.quadcopter.x, state.quadcopter.y
    vx, vy = state.quadcopter.vx, state.quadcopter.vy
    wind_vx, wind_vy = get_wind_velocity(x, y)
    relative_wind_vx, relative_wind_vy = wind_vx-vx, wind_vy-vy
    x = (
        (state.quadcopter.x, "quadcopter.x"),
        (state.quadcopter.y, "quadcopter.y"),
        (state.quadcopter.vx, "quadcopter.vx"),
        (state.quadcopter.vy, "quadcopter.vy"),
        (state.quadcopter.angle, "quadcopter.angle"),
        (state.quadcopter.angular_velocity, "quadcopter.angular_velocity"),
        (relative_wind_vx, "wind_vx"),
        (relative_wind_vy, "wind_vy"),
        (1.0 if state.load_box_i is not None else -1.0, "has_load"),
    )
    return x


def get_state_vector(state: State) -> tuple[float, ...]:
    """Return a vectorization of the given state."""
    return tuple(x for x, _ in get_vectorized_state_with_labels(state))


def get_state_vector_labels(state: State) -> list[str]:
    """Return a vectorization of the given state."""
    return [x for _, x in get_vectorized_state_with_labels(state)]


def cluster_vectors(
        states: list[tuple[float, ...]],
        min_cluster_size: int,
        ) -> list[int]:
    """Return the cluster index of each state."""
    # Vectorize states
    X = states

    # Cluster vectorized states
    # Min samples is a fraction of the dataset necessary to form a cluster
    X = preprocessing.StandardScaler().fit_transform(X)
    min_cluster_size = 80
    clustering = HDBSCAN(min_cluster_size=min_cluster_size).fit(X)
    labels = list(clustering.labels_)
    return labels


def plot_cluster_visualization(
        output_file: Path,
        states: list[State],
        labels: list[str],
        ):
    """Plot clusters."""
    cluster_n = len(set(labels))
    cluster_index = {
        cluster_name: i
        for i, cluster_name in enumerate(set(labels))
    }

    # Visualize clusters
    colormap = matplotlib.pyplot.get_cmap('tab10')
    fig = Figure(figsize=(8, 2+2*cluster_n), dpi=512)
    FigureCanvas(fig)
    ax = fig.add_subplot(cluster_n+1, 1, 1)
    ax.set_aspect('equal')
    X = list()
    Y = list()
    cs = list()
    for state, cluster_name in zip(states, labels):
        x = state.quadcopter.x
        y = state.quadcopter.y
        c = colormap(cluster_index[cluster_name])
        X.append(x)
        Y.append(y)
        cs.append(c)
    ax.scatter(X, Y, s=1, color=cs, alpha=0.3, edgecolor='none')
    ax.set_title(f"Labeled trajectory")

    for j, current_label in enumerate(set(labels)):
        nax = fig.add_subplot(cluster_n+1, 1, j+2, sharex=ax, sharey=ax)
        nax.set_aspect('equal')
        X = list()
        Y = list()
        cs = list()
        for state, cluster_i in zip(states, labels):
            if cluster_i != current_label:
                continue
            x = state.quadcopter.x
            y = state.quadcopter.y
            c = colormap(cluster_index[current_label])
            X.append(x)
            Y.append(y)
            cs.append(c)
        nax.scatter(X, Y, s=1, color=cs, alpha=0.3, edgecolor='none')
        nax.set_title(current_label)

    # Save cluster visualization
    fig.tight_layout()
    fig.savefig(output_file)


def plot_decision_tree(
        clf: tree.DecisionTreeClassifier,
        output_file: Path,
        labels: list[str],
        states: list[State],
        ):
    """Plot the given decision tree."""
    # Plot decision tree
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(1, 1, 1)
    feature_names = get_state_vector_labels(states[0])
    class_names = [i for i in set(labels)]
    tree.plot_tree(
        clf,
        ax=ax,
        feature_names=feature_names,
        class_names=class_names,
        label="root",
    )
    fig.savefig(output_file)


if __name__ == "__main__":
    # Define controller
    import argparse
    from pathlib import Path
    import torch
    parser = argparse.ArgumentParser(description='Optimize a controller.')
    parser.add_argument(
        '--controller-path',
        type=Path,
        help='Pytorch model with the initial controller weights.',
        required=True,
    )
    parser.add_argument(
        '--simulation-n',
        type=int,
        help='Number of simulations.',
        required=True,
    )
    from neuroevolution import get_untrained_policy_network
    from neuroevolution import get_controller
    network = get_untrained_policy_network()
    args = parser.parse_args()
    if args.controller_path is not None:
        network.load_state_dict(torch.load(args.controller_path))

    # Gather states with the controller
    states = list[State]()
    import random
    for _ in range(args.simulation_n):
        state = get_random_initial_state(str(random.random()))
        controller = get_controller(network)
        states.extend(get_controller_states(controller, state))

    # Cluster states
    vectors = [get_state_vector(s) for s in states]
    labels = cluster_vectors(vectors, min_cluster_size=80)

    print(f"Cluster #: {len(set(labels))}")

    # Plot clusters
    output_file = Path("clusters.png")
    str_labels = [f"Cluster {i}" for i in labels]
    plot_cluster_visualization(output_file, states, str_labels)
    print(f"Wrote {output_file}")

    # Characterize clusters with a decision tree
    X = [get_state_vector(x) for x in states]
    Y = labels
    clf = tree.DecisionTreeClassifier().fit(X, Y)

    # Plot tree
    output_file = Path(f"tree.svg")
    plot_decision_tree(clf, output_file, str_labels, states)
    print(f"Wrote {output_file}")
