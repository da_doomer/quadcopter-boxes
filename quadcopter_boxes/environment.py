"""Planar quadcopter boxes environment definition.

This environment is a dynamical system with a planar quadcopter and boxes.
Assume there exist positions A and B, with the boxes in position B. Further
assume the goal of the quadcopter is to pick up a box in position B, navigate
to position A and drop the box, then return to position B, then pickup another
box, and so on. Suppose there is a region with strong winds between point A and
B. Assume the quadcopter has a controller, even if not very efficient.

Wind is simulated with an extremely simplified model of drag applied to the
center of the quadcopter (drag equation with a constant reference area equal
to the average length of the sides of the rectangular drone).

Units are specified in the International System of Units
"""
from typing import Callable
from dataclasses import dataclass
import pymunk


Action = tuple[float, float, float]
"""Actions are four dimensional vectors with the following entries:
1. left thrust (normalized to [0, 1])
2. right thrust (normalized to [0, 1])
3. attach/detach load (engaged when > 0.0)
"""
ACTION_SIZE = 3


@dataclass(frozen=True)
class BoxState:
    """Encodes the state of a box."""
    x: float
    y: float
    vx: float
    vy: float


@dataclass(frozen=True)
class QuadcopterState:
    """Encodes the state of the quadcopter."""
    x: float
    y: float
    vx: float
    vy: float
    angle: float
    angular_velocity: float
    left_thrust: float
    right_thrust: float


@dataclass(frozen=True)
class State:
    """Encodes the state of the system."""
    boxes: list[BoxState]
    quadcopter: QuadcopterState
    load_box_i: int | None
    load_value: float


Controller = Callable[[State], Action]


@dataclass(frozen=False)
class PymunkData:
    """Data structure to perform simulation with Pymunk."""
    boxes: list[pymunk.Body]
    quadcopter: pymunk.Body
    space: pymunk.Space
    load_constraint: tuple[pymunk.Constraint, int] | None  # (constraint, box_i)
    load_value: float
    quadcopter_left_thrust: float
    quadcopter_right_thrust: float
    floor: pymunk.Shape


BOX_MASS = 0.500  # 500g
BOX_SIZE = (0.2, 0.2)  # 40cm x 40cm
QUADCOPTER_CANOPY_MASS = 1.000  # 1000g
QUADCOPTER_CANOPY_SIZE = (0.3, 0.1)  # 30cm x 10cm


def get_load_constraint(
        box_i: int,
        boxes: list[pymunk.Body],
        quadcopter: pymunk.Body) -> tuple[pymunk.Constraint, int]:
    """Return a simulation constraint that enforces the box to be a
    load to the quadcopter (pin joint constraint)."""
    constraint = pymunk.SlideJoint(
        boxes[box_i],
        quadcopter,
        anchor_a=(0.0, 0.0),
        anchor_b=(0.0, 0.0),
        min=0.0,
        max=QUADCOPTER_LOADER_MAX_DISTANCE,
    )
    return (constraint, box_i)


def get_left_thruster_relative_position() -> tuple[float, float]:
    """Return the position of the left thruster relative to the center
    of the quadcopter."""
    thruster_offset = QUADCOPTER_CANOPY_SIZE[0]/2
    return (-thruster_offset, 0.0)


def get_right_thruster_relative_position() -> tuple[float, float]:
    """Return the position of the right thruster relative to the center
    of the quadcopter."""
    thruster_offset = QUADCOPTER_CANOPY_SIZE[0]/2
    return (thruster_offset, 0.0)


SIMULATION_GRAVITY = (0, -9.82)  # 9.81m/s2
SIMULATION_DAMPING = 0.99
ARENA_X0 = 0.0   # Arena left
ARENA_X1 = 10.0  # Arena right (i.e., arena is 10 meters long)
ARENA_X0_LIMIT = ARENA_X0 - 10.0  # Out-of-bounds left
ARENA_X1_LIMIT = ARENA_X1 + 10.0  # Out-of-bounds right


def get_pymunk_simulation(state: State) -> PymunkData:
    """Return a data structure to perform the simulation with Pymunk
    starting with the given state."""
    space = pymunk.Space()
    space.gravity = SIMULATION_GRAVITY
    space.damping = SIMULATION_DAMPING

    # Add boxes
    boxes = list()
    for box_state in state.boxes:
        box = pymunk.Body()
        box.position = (box_state.x, box_state.y)
        box.velocity = (box_state.vx, box_state.vy)
        box_body = pymunk.Poly.create_box(box, size=BOX_SIZE)
        box_body.mass = BOX_MASS
        box_body.friction = 1.5
        space.add(box, box_body)
        boxes.append(box)

    # Add quadcopter
    quadcopter = pymunk.Body()
    canopy = pymunk.Poly.create_box(quadcopter, size=QUADCOPTER_CANOPY_SIZE)
    canopy.mass = QUADCOPTER_CANOPY_MASS
    canopy.friction = 1.5
    space.add(quadcopter, canopy)
    quadcopter.position = (state.quadcopter.x, state.quadcopter.y)
    quadcopter.angle = state.quadcopter.angle
    quadcopter.angular_velocity = state.quadcopter.angular_velocity
    space.reindex_shapes_for_body(quadcopter)

    # Add load constraint if appropriate
    if state.load_box_i is None:
        load_constraint = None
    else:
        load_constraint = get_load_constraint(
            state.load_box_i,
            boxes,
            quadcopter,
        )
        space.add(load_constraint[0])

    # Add floor
    floor = pymunk.Poly(
        body=space.static_body,
        vertices=[
            (ARENA_X0, 0.0),
            (ARENA_X1, 0.0),
            (ARENA_X1, -0.1),
            (ARENA_X0, -0.1),
        ],
    )
    floor.friction = 1.5
    space.add(floor)

    return PymunkData(
        boxes=boxes,
        quadcopter=quadcopter,
        space=space,
        load_constraint=None,
        quadcopter_left_thrust=state.quadcopter.left_thrust,
        quadcopter_right_thrust=state.quadcopter.right_thrust,
        load_value=state.load_value,
        floor=floor,
    )


def get_state(data: PymunkData) -> State:
    """Get the state corresponding to the given Pymunk data."""
    boxes = list()
    for box in data.boxes:
        box_state = BoxState(
            x=box.position[0],
            y=box.position[1],
            vx=box.velocity[0],
            vy=box.velocity[1],
        )
        boxes.append(box_state)

    quadcopter = QuadcopterState(
        x=data.quadcopter.position[0],
        y=data.quadcopter.position[1],
        vx=data.quadcopter.velocity[0],
        vy=data.quadcopter.velocity[1],
        angle=data.quadcopter.angle,
        angular_velocity=data.quadcopter.angular_velocity,
        left_thrust=data.quadcopter_left_thrust,
        right_thrust=data.quadcopter_right_thrust,
    )

    if data.load_constraint is None:
        load_box_i = None
    else:
        load_box_i = data.load_constraint[1]

    return State(
        boxes=boxes,
        quadcopter=quadcopter,
        load_box_i=load_box_i,
        load_value=data.load_value,
    )


DROPZONE_X0 = 0.0  # Drop zone left
DROPZONE_X1 = 2.0  # Drop zone right (i.e., drop zone is 2 meters long)

WIND_X0 = 4.0  # Wind zone left
WIND_X1 = 6.0  # Wind zone right


def get_wind_velocity(x: float, y: float) -> tuple[float, float]:
    """Get the wind force at the specified position."""
    # Average wind speed across the world ranges from 0 to 7 m/s, but it can
    # increase a lot in certain conditions.
    # https://en.wikipedia.org/wiki/Wind_speed
    #return (0.0, 0.0)
    if x < WIND_X0:
        return (-1.0, 0.0)
    if x > WIND_X1:
        return (-1.0, 0.0)
    return (2.0, 2.0)


SIMULATION_DT = 1.0/60.0
THRUST_MAX_FORCE = 10.0  # 10 Newtons per propeller (i.e., around a 2:1 thrust-to-weight ratio)
QUADCOPTER_LOADER_MAX_DISTANCE = 1.0


def step(data: PymunkData):
    """In-place dynamics step."""
    # Apply thrust
    left_force = pymunk.Vec2d(0.0,  data.quadcopter_left_thrust)
    right_force = pymunk.Vec2d(0.0, data.quadcopter_right_thrust)
    data.quadcopter.apply_force_at_local_point(
        force=left_force,
        point=get_left_thruster_relative_position(),
    )
    data.quadcopter.apply_force_at_local_point(
        force=right_force,
        point=get_right_thruster_relative_position(),
    )

    # Apply Stokes drag
    # https://en.wikipedia.org/wiki/Drag_equation
    # https://en.wikipedia.org/wiki/Drag_coefficient
    #https://en.wikipedia.org/wiki/Density 
    # https://en.wikipedia.org/wiki/Drag_(physics)#Aerodynamics
    # Reference area is "the orthographic projection of the object on a plane
    # perpendicular to the direction of motion".
    # We will simplify and use the average surface area of the
    # quadcopter as the reference area.
    mass_density = 1.2  # mass density of air
    wind_velocity = get_wind_velocity(*data.quadcopter.position)
    flow_velocity = wind_velocity-data.quadcopter.velocity
    reference_area = sum(QUADCOPTER_CANOPY_SIZE)/len(QUADCOPTER_CANOPY_SIZE)
    drag_coefficient = 1.05  # drag coefficient of a cube
    drag_component = (1/2)*mass_density*flow_velocity.get_length_sqrd()*reference_area*drag_coefficient
    drag = flow_velocity.normalized()*drag_component

    data.quadcopter.apply_force_at_local_point(
        force=drag,
        point=(0, 0),
    )

    # Engage loader if appropriate
    if data.load_value > 0.0 and len(data.boxes) > 0:
        if data.load_constraint is not None:
            # Unload loaded box
            data.space.remove(data.load_constraint[0])
            data.load_constraint = None
        else:
            # Load closest box
            quadcopter_pos = data.quadcopter.position
            def sq_distance_to_quadcopter(i: int) -> float:
                box_pos = data.boxes[i].position
                return box_pos.get_distance(quadcopter_pos)
            closest_box_i = min(
                range(len(data.boxes)),
                key=sq_distance_to_quadcopter,
            )
            if sq_distance_to_quadcopter(closest_box_i) < QUADCOPTER_LOADER_MAX_DISTANCE**2:
                constraint = get_load_constraint(
                    box_i=closest_box_i,
                    boxes=data.boxes,
                    quadcopter=data.quadcopter,
                )
                data.space.add(constraint[0])
                data.load_constraint = constraint

    data.space.step(SIMULATION_DT)


QUADCOPTER_MAX_Y = 8


def simulate(
        initial_state: State,
        controller: Controller,
        step_n: int,
        ) -> list[State]:
    """Run the simulation and return the induced sequence of states (including
    the initial state)."""
    states = list()
    simulation = get_pymunk_simulation(initial_state)
    state = get_state(simulation)
    for _ in range(step_n):
        # Get action from controller
        action = controller(state)
        left_thrust = max(0.0, min(1.0, action[0]))*THRUST_MAX_FORCE
        right_thrust = max(0.0, min(1.0, action[1]))*THRUST_MAX_FORCE
        simulation.quadcopter_left_thrust = left_thrust
        simulation.quadcopter_right_thrust = right_thrust
        simulation.load_value = action[2]

        # Step simulation
        step(simulation)
        state = get_state(simulation)
        states.append(state)

        # Interrupt simulation if the robot flies off the map
        if state.quadcopter.x < ARENA_X0_LIMIT or state.quadcopter.x > ARENA_X1_LIMIT:
            break
        if state.quadcopter.y > QUADCOPTER_MAX_Y:
            break
        # Interrupt simulation if the quadcopter crashes
        if state.quadcopter.y <= 0.0:
            break
        box_is_out_of_bounds = [
            box.y <= 0.0
            for box in state.boxes
        ]
        if any(box_is_out_of_bounds):
            break
        quadcopter_shape_collisions = [
            len(shape.shapes_collide(simulation.floor).points) > 0
            for shape in simulation.quadcopter.shapes
        ] + [
            len(shape1.shapes_collide(shape2).points) > 0
            for shape1 in simulation.quadcopter.shapes
            for box in simulation.boxes
            for shape2 in box.shapes
        ]
        if any(quadcopter_shape_collisions):
            break
    return states


if __name__ == "__main__":
    # Run simulation for debugging
    initial_state = State(
        boxes=[BoxState(x=8.0, y=BOX_SIZE[1]*2, vx=0.0, vy=0.0)],
        quadcopter=QuadcopterState(
            x=5.0,
            y=2.0,
            vx=0.0,
            vy=0.0,
            angle=0.0,
            angular_velocity=0.0,
            left_thrust=0.0,
            right_thrust=0.0,
        ),
        load_box_i=None,
        load_value=0.0,
    )
    def controller(_):
        return (0.0, 0.0, 0.0)
    states = simulate(initial_state, controller, 30)

    from plotting import plot_animation
    from pathlib import Path

    animation_path = Path("environment.py.mp4")
    plot_animation(states, animation_path, 0.1, 60, None)
    print(f"Wrote {animation_path}")
