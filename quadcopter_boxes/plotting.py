"""Plotting utilities for the environment."""
from quadcopter_boxes.environment import QUADCOPTER_MAX_Y, State
from quadcopter_boxes.environment import get_pymunk_simulation
from quadcopter_boxes.environment import ARENA_X0
from quadcopter_boxes.environment import ARENA_X1
from quadcopter_boxes.environment import get_wind_velocity
import pymunk
from pathlib import Path
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import pymunk.matplotlib_util
from pathlib import Path
from functools import reduce
import ffmpeg
import tempfile
import numpy as np


def plot_space(
        space: pymunk.Space,
        output_file: Path,
        margin: float,
        waypoint: None | tuple[float, float],
        ):
    """Plot the given space to the given output file."""
    # Create matplotlib figure
    my_dpi = 256  # Matplotlib's default DPI (100) is too low
    fig = Figure(dpi=my_dpi, tight_layout=True)
    FigureCanvas(fig)
    ax = fig.add_subplot(111)

    # Plot wind vector field before plotting simulation bodies so that
    # bodies are plotted on top of the vector field
    x_step_n = int((ARENA_X1-ARENA_X0)*5)  # approx 5 arrows per distance unit
    y_step_n = int((ARENA_X1-ARENA_X0)/QUADCOPTER_MAX_Y*x_step_n)
    arrow_x, arrow_y = np.meshgrid(
        np.linspace(ARENA_X0, ARENA_X1, x_step_n),
        np.linspace(0, QUADCOPTER_MAX_Y, y_step_n)
    )
    wind_forces = [
        get_wind_velocity(x, y)
        for xs, ys in zip(arrow_x, arrow_y)
        for x, y in zip(xs, ys)
    ]
    arrow_u = [u for u, _ in wind_forces]
    arrow_v = [v for _, v in wind_forces]
    max_force_magnitude = max([
        (u**2+v**2)**(1/2)
        for u, v in zip(arrow_u, arrow_v)
    ])
    arrow_scale = max_force_magnitude*100/2
    arrow_x = arrow_x*100
    arrow_y = arrow_y*100
    ax.quiver(arrow_x, arrow_y, arrow_u, arrow_v, color="gray", scale=arrow_scale)

    # Setup pymunk plotting utility. We scale by 100 because the simulation
    # scale is in metric units and Pymunk plotting utility expects things in
    # pixel-ish scales.
    options = pymunk.matplotlib_util.DrawOptions(ax)
    options.transform = pymunk.Transform.scaling(100.0)
    space.debug_draw(options)

    # Plot waypoint if given
    if waypoint is not None:
        ax.scatter([100*waypoint[0]], [100*waypoint[1]], marker='o')

    # Find plot limits (bounding area of the shapes in the simulation)
    bbs = [shape.bb for shape in space.shapes]
    bb = reduce(lambda b1, b2: b1.merge(b2), bbs)

    # Set plot limits
    ax.set_xlim(100*(bb.left-margin), 100*(bb.right+margin))
    ax.set_ylim(100*(bb.bottom-margin), 100*(bb.top+margin))

    # Equal aspect ratio to make plot more intuitive (1 meter has the same
    # length in both axis).
    ax.set_aspect('equal')

    # Save figure
    fig.savefig(output_file)


def plot_state(
        state: State,
        output_file: Path,
        margin: float,
        waypoint: None | tuple[float, float]
        ):
    """Plot the given state."""
    simulation = get_pymunk_simulation(state)
    plot_space(
        space=simulation.space,
        output_file=output_file,
        margin=margin,
        waypoint=waypoint,
    )


def plot_animation(
        states: list[State],
        output_file: Path,
        margin: float,
        FPS: int,
        waypoints: None | list[tuple[float, float]],
        ):
    """Plot an animation with the given states."""
    # Plot each frame
    with tempfile.TemporaryDirectory() as frame_dir:
        for i, state in enumerate(states):
            path = Path(frame_dir)/(f"{i}.png").rjust(10)
            waypoint = None if waypoints is None else waypoints[i]
            plot_state(
                state=state,
                output_file=path,
                margin=margin,
                waypoint=waypoint,
            )

        # Merge frames into animation
        (
            ffmpeg
            .input(Path(frame_dir)/"*.png", pattern_type="glob", framerate=FPS)
            .output(str(output_file))
            .overwrite_output()
            .run(quiet=True)
        )
