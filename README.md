# quadcopter-boxes

![Illustration of the task.](control/neuroevolution.webm)

A simulation of a planar quadcopter using Pymunk with boxes that have to be
moved to a target position. There is a region of wind that affects the
dynamics with a very simple model of drag.

An example controller synthesis procedure is included, using neuroevolution to
optimize a multilayer perceptron.

## Installation

Simple install with your preferred Python package manager. E.g.,

- `$ pip install .`, or,
- `$ poetry install`

Make sure `ffmpeg` is installed and in your PATH.

If you wish to execute the controller optimization, make sure CUDA is
installed.

## Project structure

```
.
|- quadcopter_boxes/
   |- environment.py        Simulation constants and dynamics
   |- plotting.py           Animation utilities
|- control/
   |- neuroevolution.py     Controller synthesis with neuroevolution
   |- visualize.py          Visualize a controller from neuroevolution.py
   |- clustering.py         Cluster observations to identify regions with continuous dynamics
   |- clustering_neuroevolution.py                Optimize a modular controller with a module for each identified cluster using a pre-trained controller
   |- iterative_clustering_neuroevolution.py      Optimize a modular controller from scratch
|- world_modelling/
   |- mlp.py                MLP-based encoder-decoder world model training
   |- mlp_example.py        MLP world model optimization example
   |- rnn.py                LSTM-based encoder-decoder world model training
   |- rnn_example.py        LSTM world model optimization example
```
