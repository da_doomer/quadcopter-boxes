"""Type definitions and data structure definitions for world modelling tasks."""
from dataclasses import dataclass


Observation = tuple[float, ...]
Trajectory = list[Observation]


@dataclass
class OptimizationLosses:
    forecasting_losses: list[float]
