"""Synthesizer to find programs that model the dynamics of a dataset
of trajectories."""
from world_modelling.types import Trajectory
from world_modelling.types import Observation
from dataclasses import dataclass
from world_modelling.neurosymbolic.dsl import ProgramInstance, Return
from world_modelling.neurosymbolic.dsl import Program
from world_modelling.neurosymbolic.dsl import If
from world_modelling.neurosymbolic.dsl import SKLearnDecisionTreePredicate
from world_modelling.neurosymbolic.dsl import AnnCall
from world_modelling.neurosymbolic.optimizer import OptimizationLosses
from world_modelling.mlp import MLPWorldModel
from world_modelling.mlp import get_optimized_world_model as get_mlp_world_model
from collections import deque
import torch
from sklearn import tree
import numpy as np
from pathlib import Path
import time
import random


@dataclass(eq=True, frozen=True)
class NeurosymbolicWorldModelSynthesisResult:
    program_instance: ProgramInstance
    losses: OptimizationLosses


@dataclass(eq=True, frozen=True)
class DatasetSplit:
    """A collection of transitions that are either well-modelled or not."""
    well_modelled: list[tuple[Observation, Observation]]
    not_well_modelled: list[tuple[Observation, Observation]]


def get_split_predicate(dataset_split: DatasetSplit) -> tree.DecisionTreeClassifier:
    """Get a predicate `P` that approximately characterizes the dataset split.

    Let `x` be a single observation.
    If `P(x) == 1`, then `x` is in the `well_modelled` set of the
    split. Otherwise, `P(x) == 0` when `x` is in the `not_well_modelled`
    set of the split.
    """
    # Build two sets of observations: one for observations in the
    # "well-modeled" partition and one for observations in the
    # "not well-modeled" partition
    positive = [
        observation
        for trajectory in dataset_split.well_modelled
        for observation in trajectory
    ]
    negative = [
        observation
        for trajectory in dataset_split.not_well_modelled
        for observation in trajectory
    ]

    # Build a decision tree to predict whether a sample is positive
    # or negative
    X = np.array(positive + negative)
    labels = [1 for _ in positive] + [0 for _ in negative]

    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(X, labels)

    return clf


def get_dataset_split(
        model: MLPWorldModel,
        trajectories: list[Trajectory],
        max_prediction_error: float,
        window_size: int,
        device: str,
        ) -> DatasetSplit:
    """Split dataset into "well modeled" and "not well modeled" transitions.

    This function receives a trajectory and outputs lists of
    consecutive observations whose transitions are either
    all well-modeled or all not well-modeled.

    A transition `(x, y)` is well-modeled
    if `(y - f(x)).norm() < max_prediction_error`.

    The split will contain all the transitions from the input trajectories,
    but the split may create trajectories. This is because a single
    trajectory in the input may contain both sub-trajectories that are well
    modelled and sub-trajectories that are not well-modelled.
    """
    # Flatten trajectories into a list of transitions
    transitions = list()
    for trajectory in trajectories:
        # Current prediction window (model takes a window and
        # outputs the next observation)
        current_window = deque([
            trajectory[0]
            for _ in range(window_size)
        ])

        for i, ti in enumerate(trajectory[:-1]):
            # Update window
            current_window.popleft()
            current_window.append(ti)

            tin = trajectory[i+1]
            transitions.append((list(current_window), ti, tin))

    # Assign each transition to one of the two sets
    well_modelled = list()
    not_well_modelled = list()

    for (wi, ti, tin) in transitions:
        # Predict next observation
        target = torch.tensor(tin)
        prediction = torch.tensor(model.predict(
            list(wi),
            n_prediction=1,
            device=device,
        ))[0]

        # Decide whether the transition is accurately modeled
        prediction_error = (prediction-target).norm().float()
        is_well_modeled = prediction_error < max_prediction_error

        transition = (ti, tin)
        if is_well_modeled:
            well_modelled.append(transition)
        else:
            not_well_modelled.append(transition)

    print(f"len(well_modelled): {len(well_modelled)}")
    print(f"len(not_well_modelled): {len(not_well_modelled)}")
    dataset_split = DatasetSplit(
        well_modelled=well_modelled,
        not_well_modelled=not_well_modelled,
    )
    return dataset_split


def get_optimized_world_model(
        trajectories: list[Trajectory],
        mlp_hidden_sizes: list[int],
        bias: bool,
        learning_rate: float,
        iter_n: int,
        batch_size: int,
        verbose: bool,
        window_size: int,
        depth_bound: int,
        device: str,
        encoded_size: int,
        min_batch_size: int,
        max_prediction_error: float,
        use_logarithmic_objective: bool,
        logarithm_constant: float,
        seed: str,
        debug_dir: Path,
        ) -> NeurosymbolicWorldModelSynthesisResult:
    """STUN-style program synthesis to find a programmatic model
    for the given dataset of trajectories.

    `logarithm_constant` is the value by which the loss is linearly
    scaled by before being passed to log when training local models, i.e.,
    `loss = log(logarithm_constant*loss)`.
    """
    assert len(trajectories) > 0
    _random = random.Random(seed)

    ann_debug_dir = debug_dir/f"logarithmic_optimization_{time.time()}"
    ann_debug_dir.mkdir()

    # Optimize a local neural module (which will model a subset of
    # the data, decided by the inductive prior of the neural network
    # and optimization algorithm)
    result = get_mlp_world_model(
        trajectories=trajectories,
        encoded_size=encoded_size,
        mlp_hidden_sizes=mlp_hidden_sizes,
        bias=bias,
        learning_rate=learning_rate,
        iter_n=iter_n,
        min_batch_size=min_batch_size,
        seed=str(_random.random()),
        verbose=verbose,
        device=device,
        window_size=window_size,
        debug_dir=ann_debug_dir,
        use_logarithmic_objective=use_logarithmic_objective,
        logarithm_constant=logarithm_constant,
    )

    # Then we split the dataset into the subsets that well-modeled and
    # not well-modeled
    dataset_split = get_dataset_split(
        model=result.model,
        trajectories=trajectories,
        max_prediction_error=max_prediction_error,
        window_size=window_size,
        device=device,
    )

    # If the model approximates the entire dataset or we are are asked
    # for the simplest program, then we return the single neural module.
    if len(dataset_split.not_well_modelled) == 0 or depth_bound == 0:
        ann_call = AnnCall(ann_index=0)
        ast = ProgramInstance(
            Program(body=Return(expression=ann_call)),
            ann_modules=[result.model],
            constant_parameters=list(),
            sklearn_trees=list(),
        )
        losses = OptimizationLosses(list())
        result = NeurosymbolicWorldModelSynthesisResult(
            program_instance=ast,
            losses=losses,
        )
        return result

    # Recursively synthesize a program for the subset of the data that is
    # not well-modeled.
    # TODO: there could be some better way to organize data, because
    # this code only works for observation window of size 1
    # but there is a way to add the observation window to each
    # transition in the split function
    assert window_size == 1, "Implementation assumption: window size is 1"
    not_well_modelled = [
        list(transition)
        for transition in dataset_split.not_well_modelled
    ]
    recursion = get_optimized_world_model(
        not_well_modelled,
        mlp_hidden_sizes=mlp_hidden_sizes,
        bias=bias,
        learning_rate=learning_rate,
        iter_n=iter_n,
        batch_size=batch_size,
        verbose=verbose,
        depth_bound=depth_bound-1,
        window_size=window_size,
        device=device,
        encoded_size=encoded_size,
        min_batch_size=min_batch_size,
        max_prediction_error=max_prediction_error,
        seed=str(_random.random()),
        use_logarithmic_objective=use_logarithmic_objective,
        logarithm_constant=logarithm_constant,
        debug_dir=debug_dir,
    )

    # Synthesize a predicate that characterizes the segmentation
    # (i.e., predicts whether the prediction should be made with the
    # local model or not).
    predicate = get_split_predicate(dataset_split)

    # Assemble synthesized program
    new_ann_index = len(recursion.program_instance.ann_modules)
    tree_index = len(recursion.program_instance.sklearn_trees)
    ann_call = Return(expression=AnnCall(ann_index=new_ann_index))
    sklearn_trees = recursion.program_instance.sklearn_trees + [
        predicate
    ]
    new_ann_modules = recursion.program_instance.ann_modules + [result.model]
    ast = Program(
        body=If(
            condition=SKLearnDecisionTreePredicate(tree_index),
            then=ann_call,
            otherwise=recursion.program_instance.ast.body,
        )
    )
    program = ProgramInstance(
        ast=ast,
        ann_modules=new_ann_modules,
        constant_parameters=recursion.program_instance.constant_parameters,
        sklearn_trees=sklearn_trees,
    )
    # TODO: actually track losses
    losses = OptimizationLosses(list())
    result = NeurosymbolicWorldModelSynthesisResult(
        program_instance=program,
        losses=losses,
    )
    return result
