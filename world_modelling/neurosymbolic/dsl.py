""""Simple neuro-symbolic DSL Abstract Syntax Tree node definitions.

```txt
S := def program(observation, constants, ann_parameters): BODY
BODY := IF | RETURN
IF := if BOOL then BODY else BODY
BOOL := BOOL and BOOL | not BOOL | EXPR > EXPR
REAL_EXPR := observation[IDX] | constants[IDX]
             | EXPR + EXPR | EXPR - EXPR | EXPR * EXPR | EXPR / EXPR
VECTOR_EXPR := ANN_CALL
ANN_CALL := neural_net(observation, ann_parameters[IDX])
RETURN := return VECTOR_EXPR
IDX := 0 | 1 | ...
```
"""
from pathlib import Path
import jsons
from dataclasses import dataclass
from typing import Union
from sklearn.tree import DecisionTreeClassifier
import torch

from world_modelling.mlp import MLPWorldModel
from joblib import dump


@dataclass(frozen=True, eq=True)
class LastObservationRead:
    """Read an index from the last observation in the window."""
    observation_index: int


@dataclass(frozen=True, eq=True)
class ConstantsRead:
    constants_index: int


@dataclass(frozen=True, eq=True)
class Addition:
    left: "RealExpression"
    right: "RealExpression"


@dataclass(frozen=True, eq=True)
class Substraction:
    left: "RealExpression"
    right: "RealExpression"


@dataclass(frozen=True, eq=True)
class Multiplication:
    left: "RealExpression"
    right: "RealExpression"


@dataclass(frozen=True, eq=True)
class Division:
    left: "RealExpression"
    right: "RealExpression"


RealExpression = Union[
    LastObservationRead,
    ConstantsRead,
    Addition,
    Substraction,
    Multiplication,
    Division,
]


@dataclass(frozen=True, eq=True)
class AnnCall:
    """neural_net(observation, ann_parameters[IDX])"""
    ann_index: int


VectorExpression = Union[
    AnnCall,
]


@dataclass(frozen=True, eq=True)
class And:
    left: "BoolExpression"
    right: "BoolExpression"


@dataclass(frozen=True, eq=True)
class Not:
    operand: "BoolExpression"


@dataclass(frozen=True, eq=True)
class LessThan:
    left: RealExpression
    right: RealExpression


@dataclass(frozen=True, eq=True)
class SKLearnDecisionTreePredicate:
    """Intended semantics: if output is class 0, then the predicate is false,
    if output is class 1, then the predicate is true."""
    index: int


BoolExpression = Union[
    And,
    Not,
    LessThan,
    SKLearnDecisionTreePredicate,
]


@dataclass(frozen=True, eq=True)
class If:
    """if BOOL then BODY otherwise BODY"""
    condition: BoolExpression
    then: "Body"
    otherwise: "Body"


@dataclass(frozen=True, eq=True)
class Return:
    """return EXPR"""
    expression: VectorExpression


Body = Union[If, Return]


@dataclass(frozen=True, eq=True)
class Program:
    body: Body


ConstantParameters = list[float]


@dataclass(frozen=True, eq=True)
class ProgramInstance:
    ast: Program
    ann_modules: list[MLPWorldModel]
    constant_parameters: ConstantParameters
    sklearn_trees: list[DecisionTreeClassifier]


def serialize_program_instance(
        program_instance: ProgramInstance,
        output_dir: Path,
        ):
    """The output directory is assumed to exist."""
    ast_path = output_dir/"ast.json"
    ann_modules_path = output_dir/"ann_modules"
    ann_modules_path.mkdir()
    trees_path = output_dir/"sklearn_trees"
    trees_path.mkdir()
    constant_parameters_path = output_dir/"constants.json"

    # Serialize AST
    with open(ast_path, "wt") as fp:
        serialized_ast = jsons.dumps(
            program_instance.ast,
            verbose=True,
            jdkwargs=dict(indent=2),
        )
        fp.write(serialized_ast)

    # Serialize PyTorch modules
    for i, model in enumerate(program_instance.ann_modules):
        module_path = ann_modules_path/f"ann_{i}.pt"
        torch.save(model, module_path)

    # Serialize constants
    with open(constant_parameters_path, "wt") as fp:
        serialized_constant_parameters = jsons.dumps(
            program_instance.constant_parameters,
            indent=2,
        )
        fp.write(serialized_constant_parameters)

    # Serialize scikit learn decision trees
    for i, tree in enumerate(program_instance.sklearn_trees):
        tree_path = trees_path/f"tree_{i}.joblib"
        dump(tree, tree_path)


ASTNode = Union[
    LastObservationRead,
    ConstantsRead,
    Addition,
    Substraction,
    Multiplication,
    Division,
    AnnCall,
    And,
    Not,
    LessThan,
    If,
    SKLearnDecisionTreePredicate,
    Return,
    Program,
]
