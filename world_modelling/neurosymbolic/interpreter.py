"""Interpreter for the neurosymbolic DSL.

The top-level functions are `evaluate` and `predict`.

At a low-level, the interpreter is split into types, meaning there is an
"interpret" function for each type.

The interpreter is modular in the sense that the interpret function
for each type is an argument, so different interpreters can be
swapped-in if needed.
"""
from world_modelling.mlp import MLPWorldModel
from world_modelling.neurosymbolic.dsl import ConstantParameters
from world_modelling.neurosymbolic.dsl import ProgramInstance
from world_modelling.neurosymbolic.dsl import BoolExpression
from world_modelling.neurosymbolic.dsl import RealExpression
from world_modelling.neurosymbolic.dsl import VectorExpression
from world_modelling.neurosymbolic import dsl
import torch
from world_modelling.windows import get_last_window
from world_modelling.types import Trajectory
from world_modelling.types import Observation
from collections import deque
from typing import Callable
from sklearn.tree import DecisionTreeClassifier


EvaluateBool = Callable[
    [
        BoolExpression,
        (Trajectory | torch.Tensor),  # Observation window
        (ConstantParameters | torch.Tensor),
        list[MLPWorldModel],  # List of ANN modules
        list[DecisionTreeClassifier],  # List of decision tree predicates
        "EvaluateBool",
        "EvaluateReal",
        "EvaluateVector",
        str,  # torch device
    ],
    bool,
]


EvaluateReal = Callable[
    [
        RealExpression,
        (Trajectory | torch.Tensor),  # Observation window
        (ConstantParameters | torch.Tensor),
        list[MLPWorldModel],  # List of ANN modules
        list[DecisionTreeClassifier],  # List of decision tree predicates
        "EvaluateBool",
        "EvaluateReal",
        "EvaluateVector",
        str,  # device
    ],
    float | torch.Tensor,
]


EvaluateVector = Callable[
    [
        VectorExpression,
        (Trajectory | torch.Tensor),  # Observation window
        (ConstantParameters | torch.Tensor),
        list[MLPWorldModel],  # List of ANN modules
        list[DecisionTreeClassifier],  # List of decision tree predicates
        "EvaluateBool",
        "EvaluateReal",
        "EvaluateVector",
        str,  # torch device
    ],
    Observation | torch.Tensor,
]


def evaluate_bool(
        node: dsl.ASTNode,
        observation_window: Trajectory | torch.Tensor,
        constant_parameters: ConstantParameters | torch.Tensor,
        ann_modules: list[MLPWorldModel],
        sklearn_trees: list[DecisionTreeClassifier],
        evaluate_bool: EvaluateBool,
        evaluate_real: EvaluateReal,
        evaluate_vector: EvaluateVector,
        device: str,
        ) -> bool:
    """If `observation_window` is tensor, output is tensor, otherwise output
    is `Observation`."""
    match node:
        case dsl.And(left=left, right=right):
            left_value = evaluate_bool(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            right_value = evaluate_bool(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return left_value and right_value
        case dsl.Not(operand=operand):
            value = evaluate_bool(
                node=operand,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return not value
        case dsl.LessThan(left=left, right=right):
            left_value = evaluate_real(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            right_value = evaluate_real(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return left_value < right_value
        case dsl.SKLearnDecisionTreePredicate(index=index):
            tree = sklearn_trees[index]
            output = tree.predict([observation_window[-1]])[0]
            return output > 0
        case other:
            raise TypeError(f"Cannot interpret node {other}!")
    raise NotImplementedError("Don't know how to evaluate yet!")


def evaluate_real(
        node: dsl.ASTNode,
        observation_window: Trajectory | torch.Tensor,
        constant_parameters: ConstantParameters | torch.Tensor,
        ann_modules: list[MLPWorldModel],
        sklearn_trees: list[DecisionTreeClassifier],
        evaluate_bool: EvaluateBool,
        evaluate_real: EvaluateReal,
        evaluate_vector: EvaluateVector,
        device: str,
        ) -> torch.Tensor | float:
    """If `observation_window` is tensor, output is tensor, otherwise output
    is `Observation`."""
    match node:
        case dsl.LastObservationRead(observation_index=observation_index):
            value = observation_window[-1][observation_index]
            return value
        case dsl.ConstantsRead(constants_index=constants_index):
            return constant_parameters[constants_index]
        case dsl.Addition(left=left, right=right):
            left_value = evaluate_real(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            right_value = evaluate_real(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return left_value + right_value
        case dsl.Substraction(left=left, right=right):
            left_value = evaluate_real(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            right_value = evaluate_real(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return left_value - right_value
        case dsl.Multiplication(left=left, right=right):
            left_value = evaluate_real(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            right_value = evaluate_real(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return left_value * right_value
        case dsl.Division(left=left, right=right):
            left_value = evaluate_real(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            right_value = evaluate_real(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return left_value / right_value
        case other:
            raise TypeError(f"Cannot interpret node {other}!")
    raise NotImplementedError("Don't know how to evaluate yet!")


def evaluate_vector(
        node: dsl.ASTNode,
        observation_window: Trajectory | torch.Tensor,
        constant_parameters: ConstantParameters | torch.Tensor,
        ann_modules: list[MLPWorldModel],
        sklearn_trees: list[DecisionTreeClassifier],
        evaluate_bool: EvaluateBool,
        evaluate_real: EvaluateReal,
        evaluate_vector: EvaluateVector,
        device: str,
        ) -> Observation | torch.Tensor:
    """If `observation_window` is tensor, output is tensor, otherwise output
    is `Observation`."""
    match node:
        case dsl.Program(body=body):
            return evaluate_vector(
                node=body,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
        case dsl.If(condition=condition, then=then, otherwise=otherwise):
            condition_value = evaluate_bool(
                node=condition,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            if condition_value:
                node = then
            else:
                node = otherwise
            value = evaluate_vector(
                node=node,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return value
        case dsl.AnnCall(ann_index=ann_index):
            ann = ann_modules[ann_index]
            if not isinstance(observation_window, torch.Tensor):
                output_tensor = False
                observation_window = torch.tensor(observation_window)
            else:
                output_tensor = True
            predicted_observation = ann.tensor_forward(
                observation_window.flatten(),
            )
            if not output_tensor:
                observation: Observation = predicted_observation.tolist()
                return observation
            return predicted_observation
        case dsl.Return(expression=expression):
            value = evaluate_vector(
                node=expression,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
            return value
        case other:
            raise TypeError(f"Cannot interpret node {other}!")
    raise NotImplementedError("Don't know how to evaluate yet!")


def evaluate(
        node: dsl.ASTNode,
        observation_window: Trajectory | torch.Tensor,
        constant_parameters: ConstantParameters | torch.Tensor,
        ann_modules: list[MLPWorldModel],
        sklearn_trees: list[DecisionTreeClassifier],
        device: str,
        ) -> Observation | torch.Tensor:
    """If `observation_window` is tensor, output is tensor, otherwise output
    is `Observation`."""
    match node:
        case dsl.Program(body=body):
            return evaluate_vector(
                node=body,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                sklearn_trees=sklearn_trees,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
                device=device,
            )
        case other:
            raise TypeError(f"Cannot interpret node {other}!")


def predict(
        trajectory: Trajectory,
        program_instance: ProgramInstance,
        n_prediction: int,
        window_size: int,
        device: str,
        ) -> list[Observation]:
    """Return the sequence of observations predicted by the program, given
    a trajectory with previous observations."""
    current_window = deque(get_last_window(
        trajectory=trajectory,
        window_size=window_size,
    ))
    predictions = list()
    for _ in range(n_prediction):
        _prediction = evaluate(
            node=program_instance.ast,
            observation_window=list(current_window),
            constant_parameters=program_instance.constant_parameters,
            ann_modules=program_instance.ann_modules,
            sklearn_trees=program_instance.sklearn_trees,
            device=device,
        )
        if isinstance(_prediction, torch.Tensor):
            prediction: Observation = _prediction.tolist()
        else:
            prediction = _prediction
        current_window.popleft()
        current_window.append(prediction)
        predictions.append(prediction)
    return predictions
