""""Neuro-symbolic world model which takes a window of observations
to predict the next state."""
from world_modelling.neurosymbolic import dsl
from world_modelling.types import Trajectory
from world_modelling.types import OptimizationLosses
from world_modelling.neurosymbolic.dsl import ProgramInstance
from world_modelling.neurosymbolic.interpreter import evaluate
from dataclasses import dataclass
from torch.nn.utils import vector_to_parameters
from torch.nn.utils import parameters_to_vector
from world_modelling.windows import get_trajectory_windows_and_targets
import torch
from evotorch.algorithms import GeneticAlgorithm
from evotorch.operators import OnePointCrossOver, GaussianMutation
from evotorch.logging import PandasLogger
from evotorch.logging import StdOutLogger
from evotorch import Problem
from statistics import mean
from pathlib import Path
import tempfile
import random
import copy


@dataclass(eq=True, frozen=True)
class NeurosymbolicWorldModelOptimizationResult:
    program_instance: ProgramInstance
    losses: OptimizationLosses


def genetic_optimizer(
        program_instance: ProgramInstance,
        trajectories: list[Trajectory],
        window_size: int,
        iter_n: int,
        population_size: int,
        worker_n: int,  # 0 or 1 means single-process
        mutation_stdev: float,
        tournament_size: int,
        batch_size: int,
        verbose: bool,
        ) -> NeurosymbolicWorldModelOptimizationResult:
    """Optimize the constants and the ANN parameters of a program instance.
    The indices will not be optimized."""
    # TODO: the cost function READS FROM DISK the ANN modules, this is super
    # slow. There must be a better way to copy the parameters
    # to each ANN module without mutating the original ANN module.
    # Before, I was simply doing copy.deepcopy, but that seemed to work
    # but it is unsupported according to the pytorch github issues.

    # We will optimize the joint parameters of all modules and constants
    # ANN modules to tempdir due to Pytorch's weird serialization semantics
    tmpdir = tempfile.TemporaryDirectory()
    ann_module_paths = list()
    for i, module in enumerate(program_instance.ann_modules):
        module_path = Path(tmpdir.name)/f"module_{i}.pt"
        torch.save(module, module_path)
        ann_module_paths.append(module_path)

    # Define functions to transform the parameters to and from a
    # vector of numbers
    def vector_to_program_instance(raw_vector: torch.Tensor) -> ProgramInstance:
        # Define a function to get the next n values in the vector
        # Reverse to pop in the same order they were added
        vector = list(reversed(raw_vector.tolist()))
        def pop_n(n: int):
            values = list()
            for _ in range(n):
                values.append(vector.pop())
            return values

        # Get constants
        new_constant_parameters = pop_n(len(program_instance.constant_parameters))

        # Get ANN modules
        new_ann_modules = list()
        for module_path in ann_module_paths:
            # Get module parameters
            ann_module_copy = torch.load(module_path)
            original_parameters = ann_module_copy.parameters()
            parameter_n = len(parameters_to_vector(original_parameters))
            parameter_vector = torch.tensor(pop_n(parameter_n))

            # This function sets the vector to the parameters
            vector_to_parameters(parameter_vector, original_parameters)

            # Record the new module
            new_ann_modules.append(ann_module_copy)

        # Get program instance
        new_instance = ProgramInstance(
            ast=program_instance.ast,
            constant_parameters=new_constant_parameters,
            ann_modules=new_ann_modules,
        )
        return new_instance

    def program_instance_to_vector(program_instance: ProgramInstance) -> torch.Tensor:
        values = list()
        values.extend(program_instance.constant_parameters)
        for ann_module in program_instance.ann_modules:
            ann_values = parameters_to_vector(ann_module.parameters())
            values.extend(ann_values.tolist())
        return torch.tensor(values)

    # Assemble regression dataset
    trajectory_windows_and_targets = list()
    for t in trajectories:
        ts = get_trajectory_windows_and_targets(
            t,
            window_size
        )
        trajectory_windows_and_targets.extend(ts)

    # Define function to evaluate candidate solutions
    def get_prediction_error(raw_vector: torch.Tensor) -> float:
        program_instance = vector_to_program_instance(raw_vector)
        batch = random.sample(trajectory_windows_and_targets, batch_size)
        targets = list()
        preds = list()
        for window, target in batch:
            prediction = evaluate(
                node=program_instance.ast,
                observation_window=window,
                constant_parameters=program_instance.constant_parameters,
                ann_modules=program_instance.ann_modules,
            )
            y_target = torch.tensor(target)
            y_pred = torch.tensor(prediction)
            targets.append(y_target)
            preds.append(y_pred)
        targets = torch.stack(targets)
        preds = torch.stack(preds)
        prediction_error = torch.nn.MSELoss()(preds, targets).item()
        return prediction_error

    # Constuct problem instance
    start_vector = program_instance_to_vector(program_instance)
    solution_length = len(start_vector)
    problem = Problem(
        "min",
        get_prediction_error,
        solution_length=solution_length,
        initial_bounds=start_vector,
        num_actors=worker_n,
    )
    searcher = GeneticAlgorithm(
        problem,
        popsize=population_size,
        operators=[
            OnePointCrossOver(problem, tournament_size=tournament_size),
            GaussianMutation(problem, stdev=mutation_stdev),
        ],
    )
    logger = PandasLogger(searcher)
    if verbose:
        # Attach an stdout logger
        StdOutLogger(searcher)

    if verbose:
        print(f"Optimizing program instance; size: {solution_length}")

    # Run optimization algorithm
    searcher.run(iter_n)

    # Record losses
    df = logger.to_dataframe()
    losses = OptimizationLosses(
        forecasting_losses=df["best_eval"].values.tolist(),
    )

    # Assemble optimized program instance
    optimized_program_instance = vector_to_program_instance(
        raw_vector=searcher.status["best"].values,
    )

    # Clean temporary directory
    tmpdir.cleanup()

    return NeurosymbolicWorldModelOptimizationResult(
        program_instance=optimized_program_instance,
        losses=losses,
    )


def gradient_ann_optimizer(
        program_instance: ProgramInstance,
        trajectories: list[Trajectory],
        window_size: int,
        iter_n: int,
        learning_rate: float,
        batch_size: int,
        verbose: bool,
        ) -> NeurosymbolicWorldModelOptimizationResult:
    """Optimize only the ANN parameters of a program instance. The indices and
    constants will not be optimized."""
    # Assemble regression dataset
    trajectory_windows_and_targets = list()
    for t in trajectories:
        ts = get_trajectory_windows_and_targets(
            t,
            window_size
        )
        trajectory_windows_and_targets.extend(ts)

    # Assemble new ANN modules
    new_ann_modules = [
        copy.deepcopy(module)
        for module in program_instance.ann_modules
    ]
    for module in new_ann_modules:
        module.train(True)

    # Initialize optimizer
    parameters = [
        param
        for module in new_ann_modules
        for param in module.parameters()
    ]
    optimizer = torch.optim.Adam(
        parameters,
        lr=learning_rate,
    )

    # Iterate gradient optimizer
    losses = list()
    loss_fn = torch.nn.MSELoss()
    for i in range(iter_n):
        # Sample batch
        batch = random.sample(trajectory_windows_and_targets, batch_size)

        # Compute predictions
        optimizer.zero_grad()
        targets = list()
        preds = list()
        for window, target in batch:
            t_window = torch.tensor(window)
            y_pred = evaluate(
                node=program_instance.ast,
                observation_window=t_window,
                constant_parameters=program_instance.constant_parameters,
                ann_modules=new_ann_modules,
            )
            y_target = torch.tensor(target)
            targets.append(y_target)
            preds.append(y_pred)
        targets = torch.stack(targets)
        preds = torch.stack(preds)

        # Compute prediction error
        prediction_error = loss_fn(preds, targets)

        loss = prediction_error.item()
        losses.append(loss)
        if verbose:
            print(f"Gradient optimizer: iter {i}; loss: {loss}")

        # Get gradient
        prediction_error.backward()

        # Step gradient optimizer
        optimizer.step()

    # Assemble optimized program instance
    optimized_program_instance = ProgramInstance(
        ast=program_instance.ast,
        constant_parameters=program_instance.constant_parameters,
        ann_modules=new_ann_modules,
    )
    losses = OptimizationLosses(
        forecasting_losses=losses,
    )
    return NeurosymbolicWorldModelOptimizationResult(
        program_instance=optimized_program_instance,
        losses=losses,
    )


def genetic_constant_optimizer(
        program_instance: ProgramInstance,
        trajectories: list[Trajectory],
        window_size: int,
        iter_n: int,
        population_size: int,
        worker_n: int,  # 0 or 1 means single-process
        mutation_stdev: float,
        tournament_size: int,
        batch_size: int,
        verbose: bool,
        ) -> NeurosymbolicWorldModelOptimizationResult:
    """Optimize the constants of a program instance.
    The indices and ANN modules will not be optimized."""
    # Define functions to transform the parameters to and from a
    # vector of numbers
    def vector_to_program_instance(raw_vector: torch.Tensor) -> ProgramInstance:
        # Get constants
        new_constant_parameters = raw_vector.tolist()

        # Get program instance
        new_instance = ProgramInstance(
            ast=program_instance.ast,
            constant_parameters=new_constant_parameters,
            ann_modules=program_instance.ann_modules,
        )
        return new_instance

    def program_instance_to_vector(program_instance: ProgramInstance) -> torch.Tensor:
        values = program_instance.constant_parameters
        return torch.tensor(values)

    # Assemble regression dataset
    trajectory_windows_and_targets = list()
    for t in trajectories:
        ts = get_trajectory_windows_and_targets(
            t,
            window_size
        )
        trajectory_windows_and_targets.extend(ts)

    # Define function to evaluate candidate solutions
    def get_prediction_error(raw_vector: torch.Tensor) -> float:
        program_instance = vector_to_program_instance(raw_vector)
        batch = random.sample(trajectory_windows_and_targets, batch_size)
        targets = list()
        preds = list()
        for window, target in batch:
            prediction = evaluate(
                node=program_instance.ast,
                observation_window=window,
                constant_parameters=program_instance.constant_parameters,
                ann_modules=program_instance.ann_modules,
            )
            y_target = torch.tensor(target)
            y_pred = torch.tensor(prediction)
            targets.append(y_target)
            preds.append(y_pred)
        targets = torch.stack(targets)
        preds = torch.stack(preds)
        prediction_error = torch.nn.MSELoss()(preds, targets).item()
        return prediction_error

    # Constuct problem instance
    start_vector = program_instance_to_vector(program_instance)
    solution_length = len(start_vector)
    problem = Problem(
        "min",
        get_prediction_error,
        solution_length=solution_length,
        initial_bounds=start_vector,
        num_actors=worker_n,
    )
    searcher = GeneticAlgorithm(
        problem,
        popsize=population_size,
        operators=[
            OnePointCrossOver(problem, tournament_size=tournament_size),
            GaussianMutation(problem, stdev=mutation_stdev),
        ],
    )
    logger = PandasLogger(searcher)
    if verbose:
        # Attach an stdout logger
        StdOutLogger(searcher)

    if verbose:
        print(f"Optimizing program instance; size: {solution_length}")

    # Run optimization algorithm
    searcher.run(iter_n)

    # Record losses
    df = logger.to_dataframe()
    losses = OptimizationLosses(
        forecasting_losses=df["best_eval"].values.tolist(),
    )

    # Assemble optimized program instance
    optimized_program_instance = vector_to_program_instance(
        raw_vector=searcher.status["best"].values,
    )

    return NeurosymbolicWorldModelOptimizationResult(
        program_instance=optimized_program_instance,
        losses=losses,
    )


def hybrid_optimizer(
        program_instance: ProgramInstance,
        trajectories: list[Trajectory],
        window_size: int,
        outer_iter_n: int,
        constants_iter_n: int,  # inner constant optimization steps
        ann_modules_iter_n: int,  # inner ANN optimization steps
        learning_rate: float,  # ANN optimization
        population_size: int,  # constant optimization
        worker_n: int,  # 0 or 1 means single-process
        mutation_stdev: float,  # constant optimization
        tournament_size: int,  # constant optimization
        batch_size: int,
        verbose: bool,
        ) -> NeurosymbolicWorldModelOptimizationResult:
    """Optimize the constants and the ANN parameters of a program instance.
    The indices will not be optimized."""
    losses = list()
    current_program_instance = program_instance
    for i in range(outer_iter_n):
        if verbose:
            print(f"Outer-loop {i}/{outer_iter_n}")
        # Optimize constants
        if constants_iter_n > 0:
            result = genetic_constant_optimizer(
                program_instance=current_program_instance,
                trajectories=trajectories,
                window_size=window_size,
                iter_n=constants_iter_n,
                population_size=population_size,
                worker_n=worker_n,
                mutation_stdev=mutation_stdev,
                tournament_size=tournament_size,
                batch_size=batch_size,
                verbose=verbose,
            )
            current_program_instance = result.program_instance
            losses.extend(result.losses.forecasting_losses)

        # Optimize ANN modules
        if ann_modules_iter_n > 0:
            result = gradient_ann_optimizer(
                program_instance=current_program_instance,
                trajectories=trajectories,
                window_size=window_size,
                iter_n=ann_modules_iter_n,
                learning_rate=learning_rate,
                verbose=verbose,
                batch_size=batch_size,
            )
            current_program_instance = result.program_instance
            losses.extend(result.losses.forecasting_losses)

    losses = OptimizationLosses(
        forecasting_losses=losses,
    )
    return NeurosymbolicWorldModelOptimizationResult(
        program_instance=current_program_instance,
        losses=losses,
    )
