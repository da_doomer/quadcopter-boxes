""""Program instance optimizer which leverages program smoothing
to optimize all parameters with gradient descent."""
from world_modelling.types import Trajectory
from world_modelling.types import OptimizationLosses
from world_modelling.neurosymbolic.dsl import ProgramInstance
from world_modelling.neurosymbolic.smooth_interpreter import evaluate_smooth
from world_modelling.neurosymbolic.optimizer import NeurosymbolicWorldModelOptimizationResult
from world_modelling.windows import get_trajectory_windows_and_targets
import torch
import random
import copy



def gradient_smooth_optimizer(
        program_instance: ProgramInstance,
        trajectories: list[Trajectory],
        window_size: int,
        iter_n: int,
        learning_rate: float,
        batch_size: int,
        verbose: bool,
        ) -> NeurosymbolicWorldModelOptimizationResult:
    """Optimize both the ANN parameters and constants of a program instance
    with gradient-based search."""
    # Assemble regression dataset
    trajectory_windows_and_targets = list()
    for t in trajectories:
        ts = get_trajectory_windows_and_targets(
            t,
            window_size
        )
        trajectory_windows_and_targets.extend(ts)

    # Assemble new ANN modules
    new_ann_modules = [
        copy.deepcopy(module)
        for module in program_instance.ann_modules
    ]
    for module in new_ann_modules:
        module.train(True)

    # Constant parameters to tensor
    tensor_constant_parameters = torch.tensor(
        program_instance.constant_parameters,
        requires_grad=True,
    )

    # Initialize optimizer
    parameters = [
        param
        for module in new_ann_modules
        for param in module.parameters()
    ]
    parameters.append(tensor_constant_parameters)
    optimizer = torch.optim.Adam(
        parameters,
        lr=learning_rate,
    )

    # Iterate gradient optimizer
    losses = list()
    loss_fn = torch.nn.MSELoss()
    for i in range(iter_n):
        # Sample batch
        batch = random.sample(trajectory_windows_and_targets, batch_size)

        # Compute predictions
        optimizer.zero_grad()
        targets = list()
        preds = list()
        for window, target in batch:
            t_window = torch.tensor(window)
            y_pred = evaluate_smooth(
                node=program_instance.ast,
                observation_window=t_window,
                constant_parameters=tensor_constant_parameters,
                ann_modules=new_ann_modules,
            )
            y_target = torch.tensor(target)
            targets.append(y_target)
            preds.append(y_pred)
        targets = torch.stack(targets)
        preds = torch.stack(preds)

        # Compute prediction error
        prediction_error = loss_fn(preds, targets)

        loss = prediction_error.item()
        losses.append(loss)
        if verbose:
            print(f"Gradient optimizer: iter {i}; loss: {loss}")

        # Get gradient
        prediction_error.backward()

        # Step gradient optimizer
        optimizer.step()

    # Assemble optimized program instance
    optimized_program_instance = ProgramInstance(
        ast=program_instance.ast,
        constant_parameters=tensor_constant_parameters.tolist(),
        ann_modules=new_ann_modules,
    )
    losses = OptimizationLosses(
        forecasting_losses=losses,
    )
    return NeurosymbolicWorldModelOptimizationResult(
        program_instance=optimized_program_instance,
        losses=losses,
    )
