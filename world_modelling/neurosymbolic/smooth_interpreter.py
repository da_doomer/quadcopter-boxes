"""Smooth interpreter for the neurosymbolic DSL.

This interpreter executes all program paths, weighing the output of different
branches by smoothing the conditional control-flow statements.

The top-level functions are `evaluate_smooth` and `predict_smooth`.
"""
from world_modelling.neurosymbolic.dsl import ConstantParameters
from world_modelling.neurosymbolic.dsl import ProgramInstance
from world_modelling.neurosymbolic import dsl
from world_modelling.neurosymbolic.interpreter import evaluate_vector as evaluate_vector_normal
from world_modelling.neurosymbolic.interpreter import evaluate_real as evaluate_real_normal
from world_modelling.neurosymbolic.interpreter import EvaluateReal
from world_modelling.neurosymbolic.interpreter import EvaluateBool
from world_modelling.neurosymbolic.interpreter import EvaluateVector
import torch
from world_modelling.windows import get_last_window
from world_modelling.types import Trajectory
from world_modelling.types import Observation
from collections import deque


def evaluate_bool_smooth(
        node: dsl.ASTNode,
        observation_window: Trajectory | torch.Tensor,
        constant_parameters: ConstantParameters,
        ann_modules: list[torch.nn.Module],
        evaluate_bool: EvaluateBool,
        evaluate_real: EvaluateReal,
        evaluate_vector: EvaluateVector,
        ) -> float | torch.Tensor:
    """Smooth predicates with quantitative semantics. A predicate is true
    if its robustness value is greater than zero."""
    match node:
        case dsl.And(left=left, right=right):
            left_value = evaluate_bool(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            right_value = evaluate_bool(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            return min(left_value, right_value)
        case dsl.Not(operand=operand):
            value = evaluate_bool(
                node=operand,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            return -value
        case dsl.LessThan(left=left, right=right):
            left_value = evaluate_real(
                node=left,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            right_value = evaluate_real(
                node=right,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            return right_value - left_value
        case other:
            raise TypeError(f"Cannot interpret node {other}!")
    raise NotImplementedError("Don't know how to evaluate yet!")



def evaluate_vector_smooth(
        node: dsl.ASTNode,
        observation_window: Trajectory | torch.Tensor,
        constant_parameters: ConstantParameters,
        ann_modules: list[torch.nn.Module],
        evaluate_bool: EvaluateBool,
        evaluate_real: EvaluateReal,
        evaluate_vector: EvaluateVector,
        ) -> Observation | torch.Tensor:
    """If `observation_window` is tensor, output is tensor, otherwise output
    is `Observation`."""
    match node:
        case dsl.If(condition=condition, then=then, otherwise=otherwise):
            condition_value = evaluate_bool(
                node=condition,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            then_value = evaluate_vector(
                node=then,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            otherwise_value = evaluate_vector(
                node=otherwise,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
            s = torch.sigmoid(condition_value)
            x1 = then_value
            x2 = otherwise_value
            value = (s)*x1 + (1-s)*x2
            return value
        case other:
            return evaluate_vector_normal(
                node=other,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool,
                evaluate_real=evaluate_real,
                evaluate_vector=evaluate_vector,
            )
    raise NotImplementedError("Don't know how to evaluate yet!")


def evaluate_smooth(
        node: dsl.ASTNode,
        observation_window: Trajectory | torch.Tensor,
        constant_parameters: ConstantParameters | torch.Tensor,
        ann_modules: list[torch.nn.Module],
        ) -> Observation | torch.Tensor:
    """If `observation_window` is tensor, output is tensor, otherwise output
    is `Observation`."""
    match node:
        case dsl.Program(body=body):
            return evaluate_vector_smooth(
                node=body,
                observation_window=observation_window,
                constant_parameters=constant_parameters,
                ann_modules=ann_modules,
                evaluate_bool=evaluate_bool_smooth,
                evaluate_real=evaluate_real_normal,
                evaluate_vector=evaluate_vector_smooth,
            )
        case other:
            raise TypeError(f"Cannot interpret node {other}!")


def predict_smooth(
        trajectory: Trajectory,
        program_instance: ProgramInstance,
        n_prediction: int,
        window_size: int,
        ) -> list[Observation]:
    """Return the sequence of observations predicted by the program, given
    a trajectory with previous observations."""
    current_window = deque(get_last_window(
        trajectory=trajectory,
        window_size=window_size,
    ))
    predictions = list()
    for _ in range(n_prediction):
        prediction = evaluate_smooth(
            node=program_instance.ast,
            observation_window=list(current_window),
            constant_parameters=program_instance.constant_parameters,
            ann_modules=program_instance.ann_modules,
        )
        current_window.popleft()
        current_window.append(prediction)
        predictions.append(prediction)
    return predictions
