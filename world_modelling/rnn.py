""""LSTM-based recurrent world model."""
import torch.nn
import torch
from dataclasses import dataclass
from pathlib import Path
import torch.nn
import torch
import random


Observation = tuple[float, ...]
Trajectory = list[Observation]


@dataclass
class LSTMWorldModel:
    """LSTM-based recurrent world model.

    Window size is the number of previous observations that are to be fed
    to the model in each step.
    """
    rnn_hidden_size: int
    encoder: torch.nn.Sequential
    rnn: torch.nn.LSTMCell
    decoder: torch.nn.Sequential
    initial_h: torch.Tensor
    initial_c: torch.Tensor

    def predict(
            self,
            trajectory: Trajectory,
            n_prediction: int,
            device: str,
            ) -> list[Observation]:
        """Predict the next observation given a trajectory. Device
        is the device where the pytorch models reside."""
        # Initialize RNN state vectors
        batch_size = 1
        h = self.initial_h.expand(batch_size, -1)
        c = self.initial_c.expand(batch_size, -1)
        X = torch.tensor(
            trajectory,
            device=device,
        )

        # Encode past trajectory
        for t in range(len(X)):
            x = X[t].unsqueeze(0)
            x_encoded = self.encoder(x)
            h, c = self.rnn(x_encoded, (h, c))

        # Predict new observations
        raw_predictions = list()
        input_x = self.encoder(X[-1].unsqueeze(0))
        for _ in range(n_prediction):
            # x stands for the current state
            h, c = self.rnn(input_x, (h, c))
            y = self.decoder(h)
            raw_predictions.append(y.squeeze())
            input_x = self.encoder(y)

        # Scale predictions to original space
        raw_predictions = torch.stack(raw_predictions).detach().cpu().numpy()
        return raw_predictions.tolist()

    def save(self, output_dir: Path):
        """Save the model to the given directory, which must exist."""
        encoder_path = output_dir/"encoder.pt"
        rnn_path = output_dir/"rnn.pt"
        decoder_path = output_dir/"decoder.pt"
        initial_h_path = output_dir/"initial_h.pt"
        initial_c_path = output_dir/"initial_c.pt"
        torch.save(self.encoder.state_dict(), encoder_path)
        torch.save(self.rnn.state_dict(), rnn_path)
        torch.save(self.decoder.state_dict(), decoder_path)
        torch.save(self.initial_h, initial_h_path)
        torch.save(self.initial_c, initial_c_path)


def get_relu_mlp(
        input_size: int,
        output_size: int,
        hidden_sizes: list[int],
        final_non_linear: bool,
        bias: bool,
        seed: str,
        ) -> torch.nn.Sequential:
    """Helper function to create an MLP with ReLU and dropout."""
    # Set Pytorch seed
    _random = random.Random(seed)
    torch.manual_seed(_random.random())

    # Instantiate MLP
    feature_sizes = list()
    for t in range(len(hidden_sizes)+1):
        in_features = input_size if t == 0 else hidden_sizes[t-1]
        out_features = output_size\
            if t == len(hidden_sizes)\
            else hidden_sizes[t]
        feature_sizes.append((in_features, out_features))

    layers = list()
    for t, (in_features, out_features) in enumerate(feature_sizes):
        layer = torch.nn.Linear(in_features, out_features, bias=bias)
        layers.append(layer)
        if t < len(feature_sizes)-1 or final_non_linear:
            #layers.append(torch.nn.BatchNorm1d(out_features))
            layers.append(torch.nn.Tanh())
            #layers.append(torch.nn.Dropout())
    return torch.nn.Sequential(*layers)


@dataclass
class OptimizationLosses:
    forecasting_losses: list[float]


@dataclass
class LSTMWorldModelOptimizationResult:
    model: LSTMWorldModel
    losses: OptimizationLosses


def get_trajectory_deltas(
        trajectory: Trajectory,
        ) -> Trajectory:
    """Return timestep-wise change."""
    deltas = list()
    X = torch.tensor(trajectory)
    for t in range(1, len(trajectory)):
        xp = X[t-1]
        x = X[t]
        delta = x - xp
        deltas.append(delta.tolist())
    return deltas


def get_optimized_world_model(
        trajectories: list[Trajectory],
        rnn_hidden_size: int,
        mlp_hidden_sizes: list[int],
        bias: bool,
        learning_rate: float,
        iter_n: int,
        min_batch_size: int,
        seed: str,
        verbose: bool,
        device: str,
        ) -> LSTMWorldModelOptimizationResult:
    """Optimize an LSTM to approximate the dynamics of the trajectories.

    All trajectories should be of equal length.

    Device is a string like 'cpu' or 'cuda'.
    """

    # Initialize models
    input_size = len(trajectories[0][0])
    output_size = input_size
    encoder = get_relu_mlp(
        input_size=input_size,
        output_size=rnn_hidden_size,
        hidden_sizes=mlp_hidden_sizes,
        bias=bias,
        final_non_linear=True,
    ).to(device)
    rnn = torch.nn.LSTMCell(
        input_size=rnn_hidden_size,
        hidden_size=rnn_hidden_size,
        bias=bias,
    ).to(device)
    decoder = get_relu_mlp(
        input_size=rnn_hidden_size,
        output_size=output_size,
        hidden_sizes=mlp_hidden_sizes,
        bias=bias,
        final_non_linear=False,
    ).to(device)

    # Initialize learnable initial hidden state
    _random = random.Random(seed)
    initial_h = torch.tensor(
        [
            (_random.random()*2-1)*1/(rnn_hidden_size)
            for _ in range(rnn_hidden_size)
        ],
        requires_grad=True,
        device=device,
    )
    initial_c = torch.tensor(
        [
            (_random.random()*2-1)*1/(rnn_hidden_size)
            for _ in range(rnn_hidden_size)
        ],
        requires_grad=True,
        device=device,
    )

    # Initialize optimizer
    params = [
        *encoder.parameters(),
        *rnn.parameters(),
        *decoder.parameters(),
        initial_h,
        initial_c,
    ]
    optimizer = torch.optim.Adam(
        params=params,
        lr=learning_rate,
    )
    optimizer.zero_grad()
    if verbose:
        print(f"Model parameters: {sum(sum(p.data.size()) for p in params)}")

    # Define loss
    loss_function = torch.nn.MSELoss()
    forecasting_losses = list()

    def step_batch(
            forecasting_predictions: list[torch.Tensor],
            forecasting_targets: list[torch.Tensor],
            ):
        # THIS METHOD HAS SIDE EFFECTS
        loss = loss_function(
            torch.stack(forecasting_predictions),
            torch.stack(forecasting_targets),
        )
        loss.backward()
        optimizer.step()
        forecasting_predictions.clear()
        forecasting_targets.clear()
        optimizer.zero_grad()
        forecasting_losses.append(loss.item())

        if verbose:
            print(f"Iter {len(forecasting_losses)} loss: {loss.item()}")

    # Iterate optimization algorithm
    _random = random.Random(seed)
    forecasting_predictions = list()
    forecasting_targets = list()
    indices = list(range(len(trajectories)))
    while len(forecasting_losses) < iter_n:
        batch_size = min_batch_size
        chosen_indices = _random.choices(indices, k=batch_size)

        # Schedule trajectory length, from short to long
        seq_len = len(trajectories[0])
        chosen_trajectories = [
            trajectories[i][:seq_len]
            for i in chosen_indices
        ]

        X = torch.tensor(chosen_trajectories, device=device)
        _, seq_len, _ = X.size()
        X_encoded = encoder(X.flatten(0, 1)).reshape(batch_size, seq_len, -1)

        # State vectors
        h = initial_h.expand(batch_size, -1)
        c = initial_c.expand(batch_size, -1)

        # Compute sequence of hidden states
        y_pred = list()
        input_x = X_encoded[:, 0, :]
        for t in range(seq_len-1):
            h, c = rnn(input_x, (h, c))
            y = decoder(h)
            y_pred.append(y)
            y_encoded = encoder(y)
            input_x = 0.5*(X_encoded[:, t+1, :] + y_encoded)

        # Change H to original (batch_size, seq_len, feature_n) format
        # and flatten for MLP decoder
        y_pred = torch.stack(y_pred).transpose(0, 1)
        forecasting_predictions.extend(y_pred.flatten(0, 1))
        forecasting_targets.extend(X[:, 1:, :].flatten(0, 1))

        step_batch(
            forecasting_predictions,
            forecasting_targets,
        )

    # Set models to evaluation mode
    encoder.train(mode=False)
    rnn.train(mode=False)
    decoder.train(mode=False)

    model = LSTMWorldModel(
        encoder=encoder,
        rnn=rnn,
        decoder=decoder,
        rnn_hidden_size=rnn_hidden_size,
        initial_h=initial_h,
        initial_c=initial_c,
    )
    losses = OptimizationLosses(
        forecasting_losses=forecasting_losses,
    )
    return LSTMWorldModelOptimizationResult(
        model=model,
        losses=losses,
    )
