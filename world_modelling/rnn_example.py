"""Example training of an LSTM world model over a simple toy dynamical
system."""
from pathlib import Path
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import random
from world_modelling.rnn import get_optimized_world_model
import torch


if __name__ == "__main__":
    # Script to check the performance on a toy dynamical system
    output_path = Path()/"rnn_output"
    output_path.mkdir()

    # Dynamics
    def step(x: tuple[float, float]) -> tuple[float, float]:
        """Toy transition function."""
        #return (x[1], x[1]*2)
        if x[0] < -1 and x[1] < 0:
            x1 = 0.1
        elif x[0] > 1 and x[1] > 0:
            x1 = -0.1
        else:
            x1 = x[1]
        return (x[0]+x[1], x1)
    def get_rollout(x: tuple[float, float], n: int) -> list[tuple[float, float]]:
        trajectory = [x]
        for _ in range(n-1):
            x = step(x)
            trajectory.append(x)
        return trajectory

    # Gather train trajectories
    _random = random.Random("asd")
    n_train_trajectories = 100_000
    trajectories = [
        get_rollout(
            (-1+2*_random.random(), _random.choice([0.1, -0.1])),
            80,
        )
        for _ in range(n_train_trajectories)
    ]

    # Approximate transition function
    result = get_optimized_world_model(
        trajectories=trajectories,
        rnn_hidden_size=32,
        mlp_hidden_sizes=[32, 32, 32],
        bias=True,
        min_batch_size=512,
        learning_rate=0.0001,
        iter_n=20_000,
        forecasting_weight=0.4,
        reconstruction_weight=0.6,
        seed="asd",
        verbose=True,
    )

    # Plot losses
    plotting_data = [
        ("Aggregate loss (train)", result.losses.aggregate_losses),
        ("Forecasting loss (train)", result.losses.forecasting_losses),
        ("Reconstruction loss (train)", result.losses.reconstruction_losses),
    ]
    for name, losses in plotting_data:
        losses_path = output_path/f"{name}.svg"
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.plot(losses)
        fig.savefig(losses_path)
        print(f"Wrote {losses_path}")

    # Plot model predictions in training data
    plot_prediction_n = 10
    test_trajectories = _random.sample(trajectories, k=plot_prediction_n)
    for i, trajectory in enumerate(test_trajectories):
        state_size = len(trajectory[0])
        y_pred = [trajectory[0]] + result.model.predict([trajectory[0]], len(trajectory)-1)
        y_target = trajectory
        y_pred = torch.tensor(y_pred)
        y_target = torch.tensor(y_target)
        plot_path = output_path/f"prediction_example_{i}.svg"
        fig = Figure()
        FigureCanvas(fig)
        axs = fig.subplots(nrows=state_size)
        for i in range(state_size):
            axs[i].plot(y_pred[:, i].tolist(), label="prediction")
            axs[i].plot(y_target[:, i].tolist(), label="target")
            axs[i].legend()
        fig.savefig(plot_path)
        print(f"Wrote {plot_path}")
