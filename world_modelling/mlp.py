""""MLP-based world model which takes a window of past observations to
predict the next observation.

This module supports optimization with a logarithmic objective to encourage
accurate modelling of local subset of the trajectories, rather than global
mediocre approximation.
"""
import torch.nn
import torch
from dataclasses import dataclass
from pathlib import Path
import torch.nn
import torch
import random
from world_modelling.rnn import get_relu_mlp
from world_modelling.types import Trajectory
from world_modelling.types import Observation
from world_modelling.types import OptimizationLosses
from world_modelling.windows import get_trajectory_windows_and_targets
from world_modelling.windows import get_last_window
from collections import deque


@dataclass
class MLPWorldModel:
    """MLP-based recurrent world model.

    Window size is the number of previous observations that are to be fed
    to the model in each step.
    """
    encoder: torch.nn.Sequential
    decoder: torch.nn.Sequential
    window_size: int

    def predict(
            self,
            trajectory: Trajectory,
            n_prediction: int,
            device: str,
            ) -> list[Observation]:
        """Predict the next observation given a trajectory. Device
        is the device where the pytorch models reside."""
        # TODO: no need to process the entire trajectory
        # Processing entire trajectory for now
        current_window = deque(get_last_window(trajectory, self.window_size))

        # Predict new observations
        raw_predictions = list()
        for _ in range(n_prediction):
            # Current window is current input
            input_x = torch.tensor(current_window, device=device).flatten()

            # Predict next state
            y = self.tensor_forward(input_x.unsqueeze(0))
            raw_predictions.append(y.squeeze())

            # Update current window
            current_window.popleft()
            current_window.append(raw_predictions[-1].tolist())

        # Scale predictions to original space
        raw_predictions = torch.stack(raw_predictions).detach().cpu().numpy()
        return raw_predictions.tolist()

    def tensor_forward(self, x: torch.Tensor) -> torch.Tensor:
        """Low-level forward."""
        y = self.decoder(self.encoder(x))
        return y

    def save(self, output_dir: Path):
        """Save the model to the given directory, which must exist."""
        encoder_path = output_dir/"encoder.pt"
        decoder_path = output_dir/"decoder.pt"
        torch.save(self.encoder.state_dict(), encoder_path)
        torch.save(self.decoder.state_dict(), decoder_path)


@dataclass
class MLPWorldModelOptimizationResult:
    model: MLPWorldModel
    losses: OptimizationLosses


def get_trajectory_deltas(
        trajectory: Trajectory,
        ) -> Trajectory:
    """Return timestep-wise change."""
    deltas = list()
    X = torch.tensor(trajectory)
    for t in range(1, len(trajectory)):
        xp = X[t-1]
        x = X[t]
        delta = x - xp
        deltas.append(delta.tolist())
    return deltas


def get_debug_losses(
        encoder,
        decoder,
        trajectory_windows_and_targets,
        loss_function,
        ):
    debug_losses = list()
    with torch.no_grad():
        for x, y_target in trajectory_windows_and_targets:
            y_pred = decoder(encoder(torch.tensor(x))).squeeze()
            loss = loss_function(y_pred, torch.tensor(y_target))
            debug_losses.append(loss.item())
    return debug_losses


def stable_loss_plot(
        losses_over_time: list[tuple[int, list[float]]],
        title: str,
        y_top_limit: float | None,
        y_bottom_limit: float | None,
        output_file: Path,
        ):
    """Helper function to plot the loss of every data point in a particular
    order."""
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    from matplotlib.figure import Figure
    fig = Figure(dpi=512)
    FigureCanvas(fig)
    ax = fig.add_subplot()

    # Plot losses in every timestamp
    X = list(range(len(losses_over_time[0][1])))
    for i, (iter_i, losses) in enumerate(losses_over_time):
        alpha = 0.5*i/len(losses_over_time) + 0.3
        ax.plot(
            X,
            losses,
            alpha=alpha,
            color="blue",
            label=f"MSE (iter {iter_i})"
        )
    ax.set_title(title)
    ax.set_ylim(bottom=y_bottom_limit, top=y_top_limit)

    # Save plot
    ax.legend()
    fig.tight_layout()
    fig.savefig(output_file)
    print(f"Wrote {output_file}")


def get_optimized_world_model(
        trajectories: list[Trajectory],
        encoded_size: int,
        mlp_hidden_sizes: list[int],
        bias: bool,
        learning_rate: float,
        iter_n: int,
        min_batch_size: int,
        seed: str,
        verbose: bool,
        device: str,
        window_size: int,
        use_logarithmic_objective: bool,
        logarithm_constant: float,
        debug_dir: Path,
        ) -> MLPWorldModelOptimizationResult:
    """Optimize an LSTM to approximate the dynamics of the trajectories.

    All trajectories should be of equal length.

    Device is a string like 'cpu' or 'cuda'.

    `logarithm_constant` is the value by which the loss is linearly
    scaled by before being passed to log, i.e.,
    `loss = log(logarithm_constant*loss)`.
    """
    _random = random.Random(seed)

    # Add history to every element in a trajectory
    trajectory_windows_and_targets = [
        (x, y)
        for t in trajectories
        for (x, y) in get_trajectory_windows_and_targets(t, window_size)
    ]

    # Initialize models
    x0, y0 = trajectory_windows_and_targets[0]
    input_size = len(torch.tensor(x0).flatten())
    output_size = len(y0)
    encoder = get_relu_mlp(
        input_size=input_size,
        output_size=encoded_size,
        hidden_sizes=mlp_hidden_sizes,
        bias=bias,
        final_non_linear=True,
        seed=str(_random.random()),
    ).to(device)
    decoder = get_relu_mlp(
        input_size=encoded_size,
        output_size=output_size,
        hidden_sizes=mlp_hidden_sizes,
        bias=bias,
        final_non_linear=False,
        seed=str(_random.random()),
    ).to(device)

    # Initialize optimizer
    params = [
        *encoder.parameters(),
        *decoder.parameters(),
    ]
    optimizer = torch.optim.Adam(
        params=params,
        lr=learning_rate,
    )
    optimizer.zero_grad()
    if verbose:
        print(f"Model parameters: {sum(sum(p.data.size()) for p in params)}")

    # Define loss
    mse_loss_function = torch.nn.MSELoss()
    stable_plot_title = (
        f"train with log({logarithm_constant}*mse)"
        if use_logarithmic_objective
        else "train with mse"
    )

    def loss_function(X, Y):
        mse_loss = mse_loss_function(X, Y)
        if use_logarithmic_objective:
            loss = torch.log(logarithm_constant*mse_loss)
            return loss
        else:
            mse_loss = mse_loss_function(X, Y)
            loss = mse_loss
            return loss
    forecasting_losses = list()

    def step_batch(
            forecasting_predictions: list[torch.Tensor],
            forecasting_targets: list[torch.Tensor],
            ):
        # THIS METHOD HAS SIDE EFFECTS
        loss = loss_function(
            torch.stack(forecasting_predictions),
            torch.stack(forecasting_targets),
        )
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        forecasting_losses.append(loss.item())

        if verbose:
            print(f"Iter {len(forecasting_losses)} loss: {loss.item()}")

    # DEBUGGING: keep track of the dataset losses
    # Track 128 random elements
    losses_over_time = list()
    tracked_datapoint_n = min(len(trajectory_windows_and_targets), 128)
    tracked_datapoint_indices = _random.sample(
        list(range(len(trajectory_windows_and_targets))),
        k=tracked_datapoint_n
    )
    tracked_datapoints = [
        trajectory_windows_and_targets[i]
        for i in tracked_datapoint_indices
    ]

    # Iterate optimization algorithm
    forecasting_predictions = list()
    forecasting_targets = list()
    indices = list(range(len(trajectory_windows_and_targets)))
    while len(forecasting_losses) < iter_n:
        # DEBUGGING: get dataset losses
        # (This could be faster but we're running on CPU)
        # (Also, only get them 10 time throughout training)
        iter_i = len(forecasting_losses)
        curve_n = 5
        plotted_iter_freq = max(1, iter_n//curve_n)
        plotted_iter_is = list(range(0, iter_n, plotted_iter_freq))
        if iter_i in plotted_iter_is:
            iter_losses = get_debug_losses(
                encoder,
                decoder,
                tracked_datapoints,
                loss_function=mse_loss_function,
            )
            losses_over_time.append((iter_i, iter_losses))

        # Sample random batch
        batch_size = min_batch_size
        chosen_indices = _random.sample(indices, k=batch_size)

        batch = [
            trajectory_windows_and_targets[i]
            for i in chosen_indices
        ]
        X_raw = list()
        Y_raw = list()
        for x, y in batch:
            X_raw.append(x)
            Y_raw.append(y)

        X = torch.tensor(X_raw, device=device).flatten(
            start_dim=1,
            end_dim=-1
        )
        Y_target = torch.tensor(Y_raw, device=device)

        # Predict with model
        Y_pred = decoder(encoder(X))

        forecasting_predictions.append(Y_pred)
        forecasting_targets.append(Y_target)

        # Step optimization algorithm
        step_batch(
            forecasting_predictions,
            forecasting_targets,
        )
        forecasting_predictions = list()
        forecasting_targets = list()

    # DEBUGGING: plot losses over time
    # Get ranking of tracked data points in last iteration
    tracked_indices = list(range(len(tracked_datapoints)))
    iteration_i_used_for_ranking = len(losses_over_time)-1
    debug_plot_order = sorted(
        tracked_indices,
        key=lambda i: losses_over_time[iteration_i_used_for_ranking][1][i],
    )
    sorted_losses_over_time = list()
    for (iter_i, loss_over_time) in losses_over_time:
        sorted_loss_over_time = [
            loss_over_time[i]
            for i in debug_plot_order
        ]
        sorted_losses_over_time.append((iter_i, sorted_loss_over_time))
    stable_loss_plot_file = debug_dir/"stable_loss_plot.png"
    stable_loss_plot(
        losses_over_time=sorted_losses_over_time,
        title=stable_plot_title,
        output_file=stable_loss_plot_file,
        y_top_limit=4,
        y_bottom_limit=-0.1,
    )

    # Set models to evaluation mode
    encoder.train(mode=False)
    decoder.train(mode=False)

    model = MLPWorldModel(
        encoder=encoder,
        decoder=decoder,
        window_size=window_size,
    )
    losses = OptimizationLosses(
        forecasting_losses=forecasting_losses,
    )
    return MLPWorldModelOptimizationResult(
        model=model,
        losses=losses,
    )
