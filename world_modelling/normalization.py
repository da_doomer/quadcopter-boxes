"""Normalization of trajectory datasets."""
import sklearn.preprocessing
from world_modelling.types import Trajectory


def get_normalizer(
        trajectories: list[Trajectory],
        seed: int,
        ) -> sklearn.preprocessing.QuantileTransformer:
    """Return a data scacler for the given trajectories."""
    # scaler = sklearn.preprocessing.MinMaxScaler((-1, 1))
    scaler = sklearn.preprocessing.QuantileTransformer(
        output_distribution='normal',
        n_quantiles=10,
        random_state=seed,
    )
    data = list()
    for trajectory in trajectories:
        data.extend(trajectory)
    scaler.fit(data)
    return scaler
