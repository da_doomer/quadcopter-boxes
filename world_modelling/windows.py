"""Trajectory window operations. A window is simply a contiguous
sub-trajectory of a trajectory."""
from world_modelling.types import Trajectory
from collections import deque


def get_last_window(
        trajectory: Trajectory,
        window_size: int,
        ) -> Trajectory:
    # TODO: no need to process the entire trajectory
    # Processing entire trajectory for now
    current_window = get_trajectory_windows(trajectory, window_size)[-1]
    return current_window


def get_trajectory_windows(
        trajectory: Trajectory,
        window_size: int,
        ) -> list[Trajectory]:
    """Return a list of contiguous sub-trajectories of the form
    [obs_{n}, ..., obs_{n+k}]."""
    windows = list()
    current_window = deque([trajectory[0] for _ in range(window_size)])
    for t in trajectory:
        current_window.popleft()
        current_window.append(t)
        windows.append(list(current_window))
    return windows


def get_trajectory_windows_and_targets(
        trajectory: Trajectory,
        window_size: int,
        ) -> list[Trajectory]:
    """Return a list of tuples of the form
    `([obs{n}, ..., obs_{n+k}], obs_{n+k+1})`."""
    windows = get_trajectory_windows(trajectory, window_size)
    windows_and_targets = list()
    for i, window in enumerate(windows[:-1]):
        target = windows[i+1][-1]
        windows_and_targets.append((window, target))
    return windows_and_targets
